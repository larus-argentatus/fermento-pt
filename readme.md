# Fermento's Website

*Submodules*:
- `hugo-embed-pdf-shortcode`:
  1. `git submodule add  https://github.com/anvithks/hugo-embed-pdf-shortcode.git themes/hugo-embed-pdf-shortcode`
  2. In `config.toml` - `theme = ["hugo-embed-pdf-shortcode", "YourCurrentTheme"] // enableInlineShortcodes = true`
- `uo-business-theme`:
  1. `git submodule add https://gitlab.com/writeonlyhugo/up-business-theme.git themes/up-business-theme`
