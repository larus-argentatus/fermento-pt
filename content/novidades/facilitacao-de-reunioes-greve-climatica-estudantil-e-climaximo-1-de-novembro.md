+++
title = "Facilitação de Reuniões | Greve Climática Estudantil e Climáximo | 1 de novembro"
date = "2023-11-02"
author = "Fermento"
type = "posts"
featured_image = "/images/formacao-facilitacao-novembro-2023.jpg"
+++

Com novas pessoas a juntar-se ao movimento pela justiça climática, os coletivos procuram aumentar a capacidade das suas ativistas para aumentar a coesão e a eficâcia.

No dia 1 de novembro, estivemos com uma dezena de ativistas na formação Facilitação de Reuniões, em que discutimos o que significa facilitar uma reunião e porque usamos facilitação, analisámos quais são as prioridades que têm nas suas reuniões, e apresentámos que ferramentas podem usar para agilizar discussão e participação.

{{< figure src="/images/formacao-facilitacao-novembro-2023.jpg" alt="Imagem da Formação" width="100%" height="auto" >}}
