+++
title = "Formação de Estratégia | Greve Climática Estudantil Lisboa | 7 de Julho"
date = "2023-07-14"
author = "Fermento"
type = "posts"
featured_image = "/images/summer-fest.jpeg"
+++

Depois da Clima Summer Fest, as ativistas da Greve Climática Estudantil Lisboa ficaram mais uns dias no espaço lindo da ABAD – Associação Bajouquense para o Desenvolvimento, em Bajouca, Leiria. Algumas formadoras do Fermento também ficaram.

Organizámos um **dia inteiro** de **formações internas** para a Greve Climática Estudantil Lisboa.

Esta **formação estratégica** teve como objetivos criar mais capacidade organizativa no grupo e trazer conhecimento para a discussão sobre prioridades estratégicas do próximo ano letivo. Fizemos análise de contexto (PESTLE), explorámos pontos de intervenção, e discutimos possíveis efeitos no espetro dos aliados.

{{< figure src="/images/summer-fest.jpeg" alt="Imagem da Formação" width="100%" height="auto" >}}

{{< figure src="/images/formacao-bajouca.jpeg" alt="Imagem da Formação" width="100%" height="auto" >}}
