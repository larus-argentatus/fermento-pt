+++
title = "Formação de Ação de Massas | Parar o Gás | 7 de maio 2023"
date = "2023-05-07"
author = "Fermento"
type = "posts"
featured_image = "/images/formacao-parar-o-gas-20230507.png"
+++


No dia 7 de maio de 2023, às 15h00 no jardim do Campo Grande, vamos estar com a plataforma de ação Parar o Gas numa Formação de Ação para preparar a ação no Terminal de GNL em Sines que vai ter lugar no dia 13 de Maio.

Aqui ficam mais informações sobre a Formação:

💥 Para a ação de Parar o Gás no dia 13 Maio no Terminal de GNL em Sines, vamos preparar-nos adequadamente, de forma a assegurar a nossa segurança e a de todas as pessoas que nos rodeiam.

🚀 Nestas Formação em Ação de Massas vais aprender e treinar várias técnicas de acção directa e criar o teu Grupo de Afinidade (grupos de 5 a 10 pessoas com a confiança e alinhamento necessário para cuidarem umas das outras antes, durante e pós ação) para assegurar o melhor desenrolar da ação e que ninguém fica para trás.

🌻 Nunca fizeste uma ação?
Este é o local e o momento para aprenderes as técnicas base, compreenderes o que é desobediência civil e como tomamos decisões coletivas na ação.

🌻 Já fizeste várias ações?
Vem treinar as técnicas essenciais para ação, partilhar experiências, integrar um grupo de afinidade e equipas de ação, tal como praticar a tomada de decisões colectivas rápidas.

🌻 Já tens um Grupo de Afinidade?
Organizem-se e participem juntas na formação!

🚀 Não é necessária inscrição e a participação é gratuita e aberta a todas as pessoas. A formação tem a duração de 3h30 sendo necessário participares na formação inteira.

[PararOGas.pt](https://pararogas.pt/)

{{< figure src="/images/formacao-parar-o-gas-20230507.png" width="100%" height="auto" >}}
