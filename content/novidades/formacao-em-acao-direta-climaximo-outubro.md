+++
title = "Formação em Ação Direta | Climáximo | Outubro"
date = "2023-10-25"
author = "Fermento"
type = "posts"
featured_image = "/images/formacao-acao-climaximo-outubro2020.jpg"
+++

As formadoras do Fermento acompanharam o Climáximo, coletivo pela justiça climática, na preparação das ativistas para o ciclo de ações que tiveram lugar ao longo de outubro.

Com estas ações, o Climáximo trouxe à agenda pública a emergência climática e o papel dos governos e das empresas no colapso civilizacional.

{{< figure src="/images/formacao-acao-climaximo-outubro2020.jpg" alt="Imagem da Formação" width="100%" height="auto" >}}
