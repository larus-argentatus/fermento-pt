+++
title = "Theories of Change and Conflict Escalation | End Fossil | Amsterdam, August 2023"
date = "2023-08-12"
author = "Fermento"
type = "posts"
featured_image = "/images/end-fossil-congress-august2023.jpg"
+++

Fermento’s trainers were in the congress of the End Fossil campaign in Amsterdam, Netherlands, where 60 organizers from 8 countries gathered.

We facilitated a workshop on Theories of Change and Conflict Escalation as part of the congress program, where we analyzed how organizations think change happens in society and, based on their understanding of social change, what strategies they use.

We are very happy to contribute to the strategic thinking process of the Occupy – End Fossil activists.

{{< figure src="/images/end-fossil-congress-august2023" alt="Imagem da Formação" width="100%" height="auto" >}}
