+++
title = "Estamos no Jornal Mapa!"
date = "2023-06-24"
author = "Fermento"
type = "posts"
featured_image = "/images/jornal-mapa-capa-20230624.png"
+++

A nova edição do Jornal Mapa (nº38 Junho-Setembro 2023) tem uma entrevista com o Fermento.

Compra o jornal e lê a nossa entrevista em conjunto com uma série de artigos inspiradores como sempre.

Mais info: https://www.jornalmapa.pt/

{{< figure src="/images/jornal-mapa-texto-20230624.png" alt="Texto da entrevista" width="100%" height="auto" >}}
