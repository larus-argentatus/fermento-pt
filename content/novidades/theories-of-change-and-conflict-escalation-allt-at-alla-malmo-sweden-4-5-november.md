+++
title = "Theories of Change and Conflict Escalation | Allt åt alla | Malmö, Sweden, 4-5 November"
date = "2023-11-05"
author = "Fermento"
type = "posts"
featured_image = "/images/malmo-nov-2023-3.jpg"
+++

Fermento hosted a training in Malmö, Sweden, at the Strategy Conference organized by the Swedish group Allt åt alla (*Everything for Everyone*, in English).

We were happy to do a training on Theories of Change and Conflict Escalation to support reflections on strategy that could feed into the discussions of the conference.

The workshop was joined by around 50 Swedish and international activist.

{{< figure src="/images/malmo-nov-2023-1.jpeg" alt="Imagem da Formação" width="100%" height="auto" >}}
{{< figure src="/images/malmo-nov-2023-2.jpg" alt="Imagem da Formação" width="100%" height="auto" >}}
{{< figure src="/images/malmo-nov-2023-3.jpg" alt="Imagem da Formação" width="100%" height="auto" >}}
