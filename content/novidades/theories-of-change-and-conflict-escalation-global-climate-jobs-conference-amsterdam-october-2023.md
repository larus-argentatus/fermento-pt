+++
title = "Theories of Change and Conflict Escalation | Global Climate Jobs Conference | Amsterdam, October 2023"
date = "2023-10-10"
author = "Fermento"
type = "posts"
featured_image = "/images/theories-of-change-and-conflict-escalation-global-climate-jobs-conference-amsterdam-october-2023-1.jpeg"
+++
Fermento’s trainers were in the Global Climate Jobs Conference in Amsterdam, Netherlands, where almost a hundred organizers gathered to talk about just transition and climate jobs. Trade unionists, grassroots climate justice activists, members of NGOs and political activists joined in the event.

We facilitated a workshop on Theories of Change and Conflict Escalation as part of the conference program, where we analyzed how organizations think change happens in society and, based on their understanding of social change, what strategies they use.

We are happy to support the strategy discussion at the intersection of labor and climate.

{{< figure src="/images/theories-of-change-and-conflict-escalation-global-climate-jobs-conference-amsterdam-october-2023-1.jpeg" alt="Imagem da Formação" width="100%" align-item="center" height="auto" >}} {{< figure src="/images/theories-of-change-and-conflict-escalation-global-climate-jobs-conference-amsterdam-october-2023-2.jpeg" alt="Imagem da Formação" align-item="center" width="100%" height="auto" >}}
