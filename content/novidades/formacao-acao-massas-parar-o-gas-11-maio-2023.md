+++
title = "Formação de Ação de Massas | Parar o Gás | 11 de maio 2023"
date = "2023-05-11"
author = "Fermento"
type = "posts"
featured_image = "/images/formacao-parar-o-gas-20230511.png"
+++


*Esta Formação em Ação de Massas acontecerá em Lisboa, no dia 11 de Maio, às 17h30, na Faculdade de Belas Artes em Lisboa.*

O protesto Parar o Porto de Gás – 13 de Maio, Sines – é divulgado publicamente e todas as pessoas, com ou sem experiência, são bem-vindas a participar. Para assegurar a nossa segurança e a de todas as que nos rodeiam, temos que estar preparadas adequadamente.

Na Formação em Ação de Massas vais aprender as técnicas base de protestos e ações com muitas pessoas, compreender o que é desobediência civil, como podes fazer resistência civil e como tomamos decisões colectivas durante a ação.

Durante a Formação também vais poder criar o teu Grupo de Afinidade (grupos de 5 a 10 pessoas com a confiança e alinhamento necessário para cuidarem umas das outras antes, durante e pós ação) para assegurar o melhor desenrolar possível!

**Já tens um Grupo de Afinidade?** Organizem-se e participem juntas na formação!

**Já fizeste várias ações?** Vem partilhar experiências, integrar um grupo de afinidade e equipas de ação tal como praticar a tomada de decisões colectivas rápidas.

Não é necessária inscrição e a participação é gratuita e aberta a todas as pessoas.

**A formação tem a duração de 3h30 sendo necessário participares na formação inteira.**

[PararOGas.pt](https://pararogas.pt/)

{{< figure src="/images/formacao-parar-o-gas-20230511.png" width="100%" height="auto" >}}
