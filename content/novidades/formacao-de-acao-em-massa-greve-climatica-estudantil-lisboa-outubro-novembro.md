+++
title = "Formação de Ação em Massa | Greve Climática Estudantil Lisboa | Outubro – Novembro"
date = "2023-10-25"
author = "Fermento"
type = "posts"
featured_image = "/images/gcelisboa_formacao-novembro-1.jpeg"
+++

Fermento vai dar várias formações de ação à Greve Climática Estudantil em preparação da onda de ações pelo Fim ao Fóssil!

Esta formação é imprescindível, para todas as pessoas que vão participar na onda, em ações dentro ou fora das escolas, e é nelas que vamos formar os grupos de afinidade para a “Visita de Estudo” ao ministério do Ambiente e da (In)ação Climática.

Datas de Formação de Ação de Massas:

- 22 de outubro, domingo, às 10h – Pavilhão de Portugal
- 28 de outubro, sábado, às 10h – Fábrica Braço de Prata
- 4 de novembro, sábado, às 10h – Fábrica Braço de Prata
- 8 de novembro, quarta-feira, às 16h30 – Pátio FBAUL
- 11 de novembro, sábado, às 10h – Fábrica Braço de Prata
- 18 de novembro, quarta-feira, às 10h – Pátio FBAUL

Se queres fazer ações disruptivas, dentro e fora da tua escola e preparar-te para a Onda e para a Visita de Estudo, vem a uma destas datas e participa na formação!

Mais informação sobre as ações e sobre a Greve Climática Estudantil Lisboa, aqui: https://greveclimaticalisboa.org/

{{< figure src="/images/gcelisboa_formacao-novembro-1.jpeg" alt="Imagem da Formação" width="100%" height="auto" >}}
{{< figure src="/images/gcelisboa_formacao-novembro-2.jpeg" alt="Imagem da Formação" width="100%" height="auto" >}}
{{< figure src="/images/gcelisboa_formacao-novembro-3.jpeg" alt="Imagem da Formação" width="100%" height="auto" >}}
