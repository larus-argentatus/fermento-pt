+++
title = "Formações de Ação | Climáximo | Novembro-Dezembro"
date = "2023-10-31"
author = "Fermento"
type = "posts"
featured_image = "/images/climaximopt_formacao-novembro-2023.jpeg"
+++

As formadoras do Fermento acompanharam o Climáximo, coletivo pela justiça climática, na preparação das ativistas para o ciclo de ações que tiveram lugar ao longo de outubro.

Com este novo ciclo de ações, o Climáximo insiste em trazer à agenda pública a emergência climática e o papel dos governos e das empresas no colapso civilizacional.

No âmbito destas formações fizemos também uma formação aberta ao público, no dia 2 de dezembro, em preparação da manifestação Resistência Climática.

Mais informações sobre as atividades do Climáximo: www.climaximo.pt

Mais informações sobre a manifestação do dia 9 de dezembro: https://www.climaximo.pt/manifestacao-resistencia-climatica/

{{< figure src="/images/climaximopt_formacao-novembro-2023.jpeg" alt="Imagem da Formação" width="100%" height="auto" >}}
