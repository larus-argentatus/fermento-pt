+++
title = "Curso Intensivo para Organizadoras do movimento por Justiça Climática | Climáximo | 18-20 de Julho"
date = "2023-07-26"
author = "Fermento"
type = "posts"
featured_image = "/images/curso-intensivo-montemor.jpg"
+++
Durante 3 dias as formadoras de Fermento estiveram num Curso Intensivo para Organizadoras do movimento por Justiça Climática em Portugal.

O Curso decorreu em Montemor-o-Novo, e foi organizado pelo Climáximo, que convidou outras organizadoras a se juntarem, e contou com as formadoras de Fermento para organizarem e participarem nas sessões.

O Curso Intensivo pretendia transformar ativistas em organizadoras, e organizadoras em organizadoras ainda mais capacitadas para fazer face ao maior desafio que a humanidade já enfrentou.

O curso pretendia explorar: Que mudança temos de provocar para travar a crise climática? Como acontece a mudança na sociedade? E como se organiza essa mudança?

Neste sentido, navegámos entre estratégia e organização com mais de 20 ativistas.

{{< figure src="/images/curso-intensivo-montemor.jpg" alt="Imagem da Formação" width="100%" height="auto" >}}
