+++
title = "Training Requests"
date = "2024-02-22"
author = "Fermento"
layout = "english_single"
+++

The trainings are aimed at organisations and have a minimum number of participants.

We don't have the capacity to set up the logistics, so we'll ask the organisation to take care of the space, registration and the physical conditions of the training.

The trainings are free. We can only ask for support for travelling, if necessary.

In operational terms:

1. A group sends us an e-mail with a request for training, ideally stating which of the four training areas and for which specific objectives.
2. We assess internally whether there is availability and ask for a 30-minute meeting with the group to adjust expectations and format.
3. We assess internally whether the request is compatible with our principles and whether we have the capacity at the moment.
4. The group takes care of the logistics of the training.

Simple, isn't it?

**Contact us via [fermento-pt@riseup.net](mailto:fermento-pt@riseup.net)**.
