+++
title = "About us"
description = "We are a training collective for social movements based in Lisbon, Portugal."
date = "2024-02-22"
author = "Fermento"
layout = "english_single"
+++

We facilitate training and workshops on organisation, strategy, communication and action, aimed at organisers and leaders of collectives, associations and civil society organisations.

We are *anti-capitalist*, *feminist* and *anti-racist*.

Our vision of activism is guided by the urgency for systemic change that the climate crisis dictates.

{{< figure src="/images/fermento-logo.png" width="100%" height="auto" alt="Logo do Fermento" >}}
