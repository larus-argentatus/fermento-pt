+++
title = "What we do"
date = "2024-02-22"
author = "Fermento"
layout = "english_single"
+++

If you're exploring this website, then you probably have one of the following doubts troubling your organisation:

- [ ] Meetings are chaotic.
- [ ] They have trouble implementing decisions and setting up organisational structures.
- [ ] New members are lost.
- [ ] There is a lack of cohesion in the group. There's a lack of clarity about who should be doing what.
- [ ] They are lost in so many activities. They don't understand what the activities are for.
- [ ] People have started to question the strategy because things don't seem to be working.
- [ ] It's not clear to everyone what the vision is and how they think they're going to get there.
- [ ] They want to do actions but they don't know how to put an action together.
- [ ] They've done actions but they went badly and they want to improve.
- [ ] They've had lots of activities but nobody has noticed them, it hasn't been in the news and there's been very little reach on social media.
- [ ] Few people are willing to speak in public or with journalists.


**Fermento is here to help you improve your organisation, strategy, action and communication.**

We have **training** designed to provide a framework for thinking about our organisations. We have **workshops** where most of the work is done by the participants to structure their groups or their strategy.

Our trainings can take anywhere from three hours to a week, depending on your needs and objectives.

Read more below, and talk to us.

---


At the moment we work in four areas: organisation, strategy, action and communication. (We also have some resources. See [here](/recursos).)

Here are some of the tools and concepts we present in our trainings.

{{< o_que_fazemos_en >}}
