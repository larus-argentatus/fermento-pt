+++
title = "How we work"
date = "2024-02-22"
author = "Fermento"
layout = "english_single"
+++



## How do I request a training?

Take a look at the [Training](/en/what-we-do) page and then follow the steps on the [Training requests](/en/training-requests) page.

---

## How to know which training to request?

You don't need to know.

You just need to be able to tell us what problem you want to see solved. If you can analyse what priority you give to that problem, that will also help us understand what training would be most appropriate.

For the rest, we'll talk in a first exploratory meeting.

---

## Is it really free?

Yes.

We just wish we didn't have to spend money out of our own pockets on materials, travelling and the like.

---

## Who funds you?

Ourselves.

We are volunteers.

---

## Who are you?

We are between 6 and 10 people, depending on how you count. We have experience in various movements (anti-austerity, human rights, student, feminist, housing, labour, climate justice) but we know each other mainly in the climate justice movement.

---

## How can I attend your trainings?

Most of our trainings are not public because they are for specific organisations.

The others you can see publicised on our [Mastodon](https://climatejustice.global/@fermento) and the [News](/novidades) page.
