+++
title = "O que fazemos"
date = "2024-02-22"
author = "Fermento"
layout = "single"
+++

Se estás a explorar este website, então muito provavelmente tens uma das seguintes dúvidas a preocupar-te na tua organização:

- [ ] As reuniões são um caos.
- [ ] Têm dificuldades em implementar as decisões e montar estruturas organizativas.
- [ ] Os novos membros estão perdidos.
- [ ] Há falhas de coesão no grupo. Falta clareza sobre quem devia fazer o quê.
- [ ] Estão perdidas em tantas atividades. Não se percebe para quê as atividades servem.
- [ ] Não é claro para todas as pessoas o que é a visão e como acham que vão chegar lá.
- [ ] As pessoas começaram a questionar a estratégia porque as coisas não parecem a funcionar.
- [ ] Querem fazer ações mas não sabem como se monta uma ação.
- [ ] Fizeram ações mas correram mal e querem melhorar.
- [ ] Tiveram muitas atividades mas ninguém reparou que aconteceram, não saiu nas notícias e nas redes sociais houve muito pouco alcance.
- [ ] Poucas pessoas estão dispostas a falar em público ou com jornalistas.


**O Fermento existe para acompanhar-vos a melhorarem a vossa organização, estratégia, ação e comunicação.**

Temos **formações** desenhadas para apresentar um enquadramento para pensar sobre as nossas organizações. Temos **oficinas** em que a maioria do trabalho é feito pelas participantes para estruturarem os seus grupos ou a sua estratégia.

As nossas formações podem demorar de três horas até uma semana, dependendo das necessidades e dos objetivos.

Lê mais abaixo, e fala connosco.

---


Neste momento trabalhamos em cinco áreas: **O**rganização, **E**stratégia, **A**ção, **C**omunicação e **S**egurança.

> (Temos também alguns recursos. Vê [aqui](/recursos).)

As nossas formações são co-desenhadas com o grupo recipiente de acordo com o seu contexto e as suas necessidades.

Aqui estão algumas ferramentas e alguns conceitos que apresentamos nas nossas formações.


---

{{< o_que_fazemos >}}
