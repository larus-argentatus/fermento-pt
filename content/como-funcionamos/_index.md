+++
title = "Como Funcionamos"
date = "2024-02-22"
author = "Fermento"
+++



## Como peço uma formação?

Vê a página das [Formações](/o-que-fazemos) e depois segue os passos na página dos [Pedidos de formações](/pedidos-formacoes).

---

## Como vou saber que formação pedir?

Não precisas de saber.

Tens é de saber dizer-nos qual é o problema que queres ver resolvido. Se puderes analisar qual é a prioridade que vocês dão a esse problema, isso também ajuda-nos a perceber que formação seria mais adequada.

O resto, falamos numa primeira reunião exploratória.

---

## É mesmo gratuito?

Sim.

Gostaríamos só de não ter de gastar dinheiro do nosso bolso para os materiais, as deslocações, e afins.

---

## Quem vos financia?

Nós.

Somos voluntárias.

---

## Mas quem são vocês mesmo?

Somos entre 6 e 10 pessoas, dependendo da forma de contar. Temos experiência em vários movimentos (anti-austeridade, direitos humanos, estudantil, feminismo, habitação, laboral, justiça climática) mas conhecemo-nos principalmente no movimento pela justiça climática.

---

## Como posso assistir as vossas formações?

A maioria das nossas formações não são públicas porque são para organizações específicas.

As outras, podes ver divulgadas no nosso [Mastodon](https://climatejustice.global/@fermento) e na página das [Novidades](/novidades).
