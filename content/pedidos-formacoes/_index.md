+++
title = "Pedidos de Formações"
date = "2024-02-22"
author = "Fermento"
+++

As formações são dirigidas às organizações e têm um número mínimo de participantes.

Não temos capacidade para montar a logística, por isso vamos pedir à organização para se tratar do espaço, das inscrições e das condições físicas da formação.

As formações são gratuitas. Podemos só pedir apoio para deslocações, se for necessário.

Em termos operacionais:

1. Um grupo envia-nos e-mail com o pedido de formação, idealmente dizendo já qual das quatro áreas de formação e para que objetivos específicos.

2. Avaliamos internamente se existe disponibilidade e pedimos uma reunião de 30 minutos com o grupo para ajustar expetativas e formato.

3. Avaliamos internamente se o pedido é compatível com os nossos princípios e se temos capacidade no momento.

4. O grupo trata da logística da formação.

Simples, não é? Contacta-nos via [fermento-pt@riseup.net](mailto:fermento-pt@riseup.net) .
