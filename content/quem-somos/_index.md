+++
title = "Quem Somos"
description = "Somos um coletivo de formações para movimentos sociais, baseado em Lisboa, Portugal."
date = "2024-02-22"
author = "Fermento"
+++

Facilitamos formações e oficinas em organização, estratégia, comunicação e ação, dirigidas às organizadoras e dirigentes dos coletivos, associações e organizações de sociedade civil.

Somos *anti-capitalistas*, *feministas* e *anti-racistas*.

A nossa visão de ativismo é guiada pela urgência de mudança sistémica que a crise climática dita.

{{< figure src="/images/fermento-logo.png" width="100%" height="auto" alt="Logo do Fermento" >}}
