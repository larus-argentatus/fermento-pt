+++
title = "KoKo, Germany"
date = "2024-02-22"
author = "Fermento"
locals = "internacional"
featured_image = "/images/coletivos/koko.png"
+++


Our Communication-Collective is made up of a handful of young and experienced trainers. We have all been active in grassroots groups and social movements for years. This has sparked our interest in group process and group dynamics, so that we worked on topics such as how to overcome hidden hierarchies and how to deal with obstacles in communication and consensus decision making.

We want to support people and groups that are implementing their vision in an equal and self-organised manner. That’s why we would like to introduce tools and open the space that allow participants to come closer to their aims.

We offer workshops, facilitation and support for the topics of communication, group development, (consensus) decision making, campaign and project planning, as well as action trainings.

https://www.kommunikationskollektiv.org/

{{< figure src="/images/coletivos/koko.png" alt="KoKo, Germany" align="center" width="100%" height="auto">}}
