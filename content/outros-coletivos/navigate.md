+++
title = "Navigate, UK"
date = "2024-02-22"
author = "Fermento"
locals = "internacional"
featured_image = "/images/coletivos/navigate.png"
+++

Navigate is a team of experienced facilitators and trainers. We facilitate meetings, run workshops and mediate conflicts for groups and organisations working towards social and environmental justice across the UK. Our team can help you to work better together, explore power dynamics, and find a way through challenges and disagreements. We can support you to cultivate sustainable working practices, make stronger collaborative decisions, design your strategy and develop your group’s processes.

https://www.navigate.org.uk/

{{< figure src="/images/coletivos/navigate.png" alt="Navigate, UK" align="center" width="100%" height="auto">}}
