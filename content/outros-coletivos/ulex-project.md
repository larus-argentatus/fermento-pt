+++
title = "Ulex Project, Catalunya, Spain"
date = "2024-02-22"
author = "Fermento"
locals = "internacional"
featured_image = "/images/coletivos/ulex-project.png"
+++


- high-quality trainings building social movement capacity for social justice and ecological integrity
- a residential training centre serving the needs of social movements for the long haul
- collaboration and innovation enabling the responsive development of social movement training in Europe
- a hub strengthening connections for pan-European solidarity and social movement resilience

https://ulexproject.org/

{{< figure src="/images/coletivos/ulex-project.png" alt="Ulex Project, Catalunya, Spain" align="center" width="100%" height="auto">}}
