+++
title = "Skills for Action, Germany"
date = "2024-02-22"
author = "Fermento"
locals = "internacional"
featured_image = "/images/coletivos/skills-for-action.png"
+++


For successful actions – not only but especially for actions of civil disobedience – we need good preparation.  Learning from past experiences can be helpful and action techniques can also be learned! Action training workshops are a space where both can happen. In such workshops, participants can exchange experiences from past actions and practice blockade and other action techniques as well as quick collective decision-making in stressful situations.

https://skillsforaction.blackblogs.org

> NB. Skills for Action also has an enormous library of original resources for actions and action trainings, [here](https://skillsforaction.blackblogs.org/en/material-2/).

{{< figure src="/images/coletivos/skills-for-action.png" alt="Skills for Action, Germany" align="center" width="100%" height="auto">}}
