+++
title = "Spina, Poland"
date = "2024-02-22"
author = "Fermento"
locals = "internacional"
featured_image = "/images/coletivos/spina.png"
+++

We are an informal collective of trainers. We support grass-root groups in the struggle for sustainable, long-lasting, radical change. We facilitate workshops on:

– consensus
– group processes
– non-hierarchical structures
– conflict resolution
– activist burnout
– emotional support
– trauma support
– sustainable activism

https://spina.noblogs.org/

{{< figure src="/images/coletivos/spina.png" alt="Spina, Poland" align="center" width="100%" height="auto">}}
