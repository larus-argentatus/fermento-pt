+++
title = "Seeds for Change, UK"
date = "2024-02-22"
author = "Fermento"
locals = "internacional"
featured_image = "/images/coletivos/seeds-for-change.png"
+++

Seeds for Change exists to support groups who are trying to do just that, and who share our core values of equality, freedom and solidarity – for human beings, other animals and the ecosystems we are part of.

We are a workers’ co-op of experienced campaigners and co-operators. We offer training, facilitation, online resources and other support for campaigns, community groups and co-operatives.

https://seedsforchange.org.uk/

{{< figure src="/images/coletivos/seeds-for-change.png" alt="Seeds for Change, UK" align="center" width="100%" height="auto">}}
