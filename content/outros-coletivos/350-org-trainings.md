+++
title = "350.org Trainings, USA"
date = "2024-02-22"
author = "Fermento"
locals = "internacional"
featured_image = "/images/coletivos/350.png"
+++

- Training/Workshop Activities
- Meeting Facilitation Tools
- Strategy + Organizing

https://trainings.350.org/

{{< figure src="/images/coletivos/350.png" alt="350.org Trainings, USA" align="center" width="100%" height="auto">}}
