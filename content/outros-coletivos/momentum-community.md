+++
title = "Momentum Community, USA"
date = "2024-02-22"
author = "Fermento"
locals = "internacional"
featured_image = "/images/coletivos/momentum-community.png"
+++

Momentum is a training institute and movement incubator. We give progressive organizers the tools and frameworks to build massive, decentralized social movements.

By combining and systematizing best practices from past movements, critical movement theory, and applicable skills, we encourage organizers to dream about big change, and give them the tools to make it happen.

https://www.momentumcommunity.org/

{{< figure src="/images/coletivos/momentum-community.png" alt="Momentum Community, USA" align="center" width="100%" height="auto">}}
