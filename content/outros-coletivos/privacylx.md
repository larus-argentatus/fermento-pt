+++
title = "Privacy LX"
date = "2024-02-22"
author = "Fermento"
locals = "portugal"
featured_image = "/images/coletivos/privacylx.png"
+++


A PrivacyLx é uma associação sem fins lucrativos focada na defesa da privacidade e segurança digital como direitos fundamentais para uma sociedade livre.

Foca-se na educação, investigação e desenvolvimento de ferramentas para a sua proteção. Tem organizado diversos eventos para o público em geral e alguns mais dirigidos a grupos cuja segurança digital está mais ameaçada.

https://privacylx.org/

{{< figure src="/images/coletivos/privacylx.png" alt="Privacy LX" align="center" width="100%" height="auto">}}
