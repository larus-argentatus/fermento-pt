+++
title = "New Economy Organisers Network – NEON, UK"
date = "2024-02-22"
author = "Fermento"
locals = "internacional"
featured_image = "/images/coletivos/neon.png"
+++

NEON offers hands-on support and training for campaigners, organisers, communications and operations teams working across social movements. They also have tools and resources that help build stronger movements primed to win, such as frameworks, guides, toolkits and coaching opportunities in areas ranging from messaging and framing to operations.

https://www.neweconomyorganisers.org/

{{< figure src="/images/coletivos/neon.png" alt="New Economy Organisers Network – NEON, UK" align="center" width="100%" height="auto">}}
