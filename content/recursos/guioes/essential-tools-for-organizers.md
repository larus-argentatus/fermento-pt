+++
title = "Essential Tools for Organizers"
date = "2023-07-26"
author = "Fermento"
type = "posts"
featured_image = "/images/recursos/guiao-essential-tools-for-organizers.png"
recursos = "guiões"
weight = "3"
+++

In this note, we’ll give you a total of 16 tools for social movements.

They are divided into 2 categories. **(1) Organization**, related to what people also call Structure, Process, or Internal. (so this is not about Community Organizing) **(2) Mobilization**, related to Strategy and Campaigning. Each will have 8 tools.

They are divided into 2 groupings. **(1) Essentials**: tools that you need on a daily basis, that you should always have in mind, in every meeting and in every conversation and on every task. **(2) Important:** tools that you need on a regular basis, perhaps weekly or biweekly, to plan and prepare your activities. Basically, **the Essentials should be at your hand at all times and the Important should be at your pocket at all times**.

So that makes 4+4 Organization tools and 4+4 Mobilization tools. Here is the full list. Further below we will briefly explain what they are and give you references on where you can learn more.

* * *

**Organization Essentials**

1.  [Six Leadership Styles](#o1-six-leadership-styles)
2.  [RACI organizing](#o2-raci-organizing)
3.  [Table of Meeting Priorities](#o3-table-of-meeting-priorities)
4.  [Delegating Work](#o4-delegating-work)

**Mobilization Essentials**

1.  [Spectrum of Allies](#m1-spectrum-of-allies)
2.  [Cycle of Momentum](#m2-cycle-of-momentum)
3.  [Action Learning Cycle](#m3-action-learning-cycle)
4.  [Action Star](#m4-action-star)

---

**Organization Important Tools**

5.  [Checklist for Preparing a Meeting](#o5-checklist-for-preparing-a-meeting)
6.  [Checklist Media Team](#o6-checklist-media-team)
7.  [Four Organizational Layers](#o7-four-organizational-layers)
8.  [Ladder of Engagement](#o8-ladder-of-engagement)

**Mobilization Important Tools**

5.  [SMART objectives](#m5-smart-objectives)
6.  [Action Briefing](#m6-action-briefing)
7.  [Stay on Message](#m7-stay-on-message)
8.  [Points of Intervention](#m8-points-of-intervention)

---

So let’s start with the essentials.

## Essential tools for Organizers

---

### O1. Six Leadership Styles

We define **leader** as **someone who makes good proposals or who can identify a good proposal when they see one**. This means that leadership is relevant in all contexts.

In any collaborative space, you need to understand the needs of the group and of the individuals of the group, as well as external factors like approaching deadlines or mandate limitations of the group. That means you need to understand the context in which proposals are being made and received.

We propose you start with [this](https://www.youtube.com/watch?v=tYW6X5qwnMw) **introduction to leadership typologies**.

{{< youtube tYW6X5qwnMw >}}

You can then learn about the Six Leadership Styles based on emotional intelligence, in [this playlist](https://www.youtube.com/playlist?list=PLvTjm2aA47rTryRLrZC-IJhmEZPjYdutc). The six styles are Affiliative, Coaching, Commanding, Democratic, Pace-setting, and Visionary.

The more you use and think about the leadership styles, the more nuances you will discover about them.

We generally present the Six Leadership Styles in our two-day courses as part of organizational thinking.

---

### O2. RACI Organizing


RACI stands for

*   Responsible
*   Accountable
*   Consult
*   Inform

and it’s a tool to organize roles and responsibilities in any team.

It clarifies team structure and mandates. It also gives the flowchart of decision-making because it gives which person/team should be consulted before making a decision and which person/team should be informed after making a decision.

RACI is a very easy-to-use tool. There are loads of resources online, most of them are quite good. You can start with [this one](https://www.teamgantt.com/blog/raci-chart-definition-tips-and-example), for example. In any case, in our experience, you won’t be able learn much until you personally engage with it.

---

### O3. Table of Meeting Priorities

Most of organizational work takes place in meetings. Consequently, most of our frustrations also arise from meetings. We developed a gamification tool that identifies group needs from a meeting. The table looks at various aspects and forces people to speak up what is essential and what is optional for them.

We look at Meeting agenda, Facilitation, Physical conditions, Progress, Process, Content, Timing, Documentation and Meeting environment. Here is how the empty table looks like, in Portuguese.

{{< figure src="/images/recursos/prioridades-reunioes.jpg" align="center" width="100%" >}}

After explaining each item, we require each individual to fill in minimum 14 and maximum 15 points in the table. After some more steps, we have a group discussion on whether the meetings correspond to group needs and what should be improved.

We introduce this tool in our Meeting Facilitation trainings as well as a stand-along activity in any meeting.

---

### O4. Delegating Work


Every time we set up a team, roles and tasks are distributed. Only in rare cases do everyone know what exactly the domain of their role is. Only in rarer cases do people have access to all the resources and relevant skills. This means that most of the time, we are delegating work and work is being delegated to us.

Our experience is that in social movements, this is done in a lousy way. (Sometimes this lousiness is justified as “empowerment” because, people say, delegation means a hierarchy and instead of delegating one should just let the person take the initiative on their own.) Instead, we propose to handle it intentionally.

There are loads of guides, articles and videos online. Many are managerial and you will have to read them with a grain of salt. Most will still be useful.

We have an entire 2-hour training on direct-training, coaching and delegation at work.

---
---

### M1. Spectrum of Allies


Sometimes also called Spectrum of Support, this tools helps you locate where the movement actors, various demographic groups and influential social actors are in relation to your movement. It also helps you to analyze to what extent your actions polarize the society or the movement.

**Beautiful Trouble** has a wonderful explanation of what it is and how to use it. Check it out [here](https://beautifultrouble.org/toolbox/tool/spectrum-of-allies/).

{{< figure src="https://assets.beautifultrouble.org/medium-SPECTRUMOFALLIES.png" width="100%" align="center" >}}

We use the Spectrum of Allies in almost all our strategy trainings, and recommend groups to consult it every time they prepare an action, an event or a press release.

---

### M2. Cycle of Momentum


Developed by the **Momentum Community**, the [Cycle of Momentum](https://www.momentumcommunity.org/new-page) helps group to strategize in a way that helps build a movement. The Cycle identifies active popular support, escalation and absorption stages.

{{< figure src="https://lh5.googleusercontent.com/BmRwXXxvyOee43iQtA0WkVCRZQVjcWvBY5_oTkKatXwqJ4g27iI74q2vZFTXZaK9cpEYTVaMFVX-K-CD2AfeSaBahLOjSR6-ocB04hWxDImarMZ_OaXodQ8fjUTIvyKJeL9_2cUd" align="center" width="100%" >}}

You can learn about the Cycle of Momentum [in this article](https://movementmetricsresearch.gitbooks.io/movement-metrics-research/content/chapter1/doing-the-work-cycles-of-momentum.html).

You should also watch this online training to understand how it works and how to use it.

{{< youtubepl PLeJeAirMA52rCePt4WuuZPD1WXb2Jnd5H >}}

---

### M3. Action Learning Cycle


Although there are many online examples of this tool, the one we found most useful for social movements was designed by the **Ulex project**.

{{< figure src="https://arquivo.climaximo.pt/wp-content/uploads/2019/12/Action-Learning-Cycle-small-768x1002.jpg" width="100%" align="center">}}

While the Cycle of Momentum helps you produce an expansive cycle with the general public and getting more people into the movement, the Action Learning Cycle describes a simultaneous intra-organizational cycle with which the group learns from its activities and deepens its understanding of the sociopolitical context in which it operates.

---

### M4. Action Star


This incredible tool developed by **Beautiful Trouble** gives a comprehensive checklist for action preparations.

{{< figure src="https://assets.beautifultrouble.org/medium-methodology-action-star-diagram.jpg" width="100%" align="center">}}

You can learn more about how to use the Action Star, [here](https://beautifultrouble.org/toolbox/tool/action-star/).

---
---

These were the top 8 tools for organizers. You should have them at hand at all times. There are another 8 tools that we think you should have in your pocket, always available to use because you’ll need them regularly and frequently. The difference between the first 8 and the second 8 is that you don’t have to be using the latter tools all the time, continuously,

---

## Important tools for Organizers

---

### O5. Checklist for Preparing a Meeting

Many meetings that are ineffective or inefficient are so because people prepared them poorly. This is easy to solve if you have checklist to go through. There are obviously plenty of such lists online. **Climáximo** prepared one that works for the context of social movements.

You can find it [here](/recursos/checklist/preparing-meeting).

---

### O6. Checklist Media Team

Every time we organize a public event or an action, there is a media team. Sometimes it’s just one person, sometimes a lot of people are part of it. Sometimes they know what they are doing, most of the times they don’t.

**Climáximo** prepared a step-by-step checklist for setting up a media team and running it.

You can find it [here](/recursos/checklist/media-team).

---

### O7. Four Organizational Layers

Many groups have difficulty in establishing cohesion and coherency because the members don’t know if their baseline agreement is ideological, political, strategic or tactical.

We highly recommend a group discussion of the following short article.

> *[Four organizational layers: a guide for grassroots activists on organizing and organizations](https://arquivo.climaximo.pt/2019/01/01/four-organizational-layers-a-guide-for-grassroots-activists-on-organizing-and-organizations/)*

We also facilitate group discussions on this topic in our two-day organizational trainings.

---

### O8. Ladder of Engagement

This is one of those tools that are really useful but if you look it up online you will only find corporate and commercial examples. **350.org** did prepare a short explanation that works for social movement contexts (you can find it below and [here](https://trainings.350.org/resource/increase-your-volunteers-involvement/)), but our intuition is that you will have to design the ladder of your organizations on your own.

> *[Increase Your Volunteers’ Involvement: Using the Ladder of Engagement](https://trainings.350.org/resource/increase-your-volunteers-involvement/)*

It should ideally **start with visibility and go through followers, members, volunteers, activists, and leaders**. At any stage of the ladder you may or may not decide to formalize engagement. (Some groups have formal membership. Some groups have formal leadership. Some groups define these in a more fluid way.) We have seen groups using ladders with 5 stages, we have also seen groups identifying 9 distinct stages.

The Ladder is also useful to highlight the difference (and overlap) between mobilization and onboarding.

We have a one-day-long training dedicated to building a Ladder of Engagement for organizations.

---
---

### M5. SMART Objectives

Although there are many versions of this, we use SMAAARRT objectives in our strategy trainings.

That stands for

-   Specific
-   Measurable
-   Ambitious
-   Achievable
-   Actionable
-   Relevant
-   Realistic
-   Timed

We have a full explanation of what they are, what they are not and how to use them. We also have a table for team leaders to fill when they plan activities. You can find the guidelines [here](/recursos/checklists/smaaarrt-objectives-for-activists/) and the table [here](/recursos/checklists/smaaarrt-objectives).

> [SMAAARRT Objectives for Activists](/recursos/checklists/smaaarrt-objectives-for-activists/)

> [SMAAARRT Objectives Checklist](/recursos/checklists/smaaarrt-objectives)

---

### M6. Action Briefing

We have seen so many meetings go awry and so many action become increasingly confusing. One big reason for this is that the proposal is not presented well in the beginning. (As one friend of ours said: The best way to spoil a good idea is to present it poorly.)

When you present a proposal (we call it an action, but it may well be any activity), you should **carefully go through the Context, the Concept, the Image and the Details**, in this order.

Climáximo prepared a manual for how you can prepare your Action Briefing, with several concrete examples. You can find it [here](/recursos/guioes/action-briefing).

---

### M7. Stay on Message

This principle, as presented by the **Beautiful Trouble**, can be defined as follows:

“_Message discipline is the art of communicating what you set out to communicate, clearly, memorably, and consistently._“

You can learn more about it [here](https://beautifultrouble.org/toolbox/tool/stay-on-message).

We present and practice this principle in our communication trainings. It is essential not only for press officers and spokespersons but also for anyone interacting with the public in any form.

---

### M8. Points of Intervention


Points of Intervention are where the status quo reproduces itself and therefore where your group can cause disruption. **Beautiful Trouble** developed a framework to understand the Points of Intervention. They identify **points of Assumption, Decision, Opportunity, Destruction, Production and Consumption**.

{{< figure src="https://assets.beautifultrouble.org/medium-POINTSOFINTERVENTION.png" align="center" width="100%" >}}

We introduce Points of Intervention in our trainings as one of the basic strategy tools. As you may have noticed, it’s also part of the Action Star.

You can learn more about the Points of Intervention [here](https://beautifultrouble.org/toolbox/tool/points-of-intervention/).

---
---
---

While there are many more tools, our claim is that if you are using these 4+4 tools effectively, you will be more than comfortable in organizing and strategizing. We have no claims over any of these tools and they are all “copy-left”.

If your organization needs support in understanding better any of these tools or in putting them in practice, contact us to set up a training.
