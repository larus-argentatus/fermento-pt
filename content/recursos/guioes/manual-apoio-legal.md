+++
title = "Manual de Apoio Legal"
date = "2023-07-26"
author = "Fermento"
type = "posts"
featured_image = "/images/recursos/guiao-manual-apoio-legal.png"
recursos = "guiões"
weight = "1"
+++

*See below for English.*

## Manual de Apoio Legal

O sistema jurídico é um **pilar de apoio** do sistema atual.

Neste sistema, destruir o planeta habitável chama-se negócio, parar essa destruição chama-se crime. Isto significa que as leis são escritas e os juízes formados para manter o sistema como está. (O mesmo princípio aplica-se à polícia.)

A nossa aposta não é que os juízes (ou os polícias) vão dar razão às ativistas e vão colaborar. Isso seria contra o ADN destas instituições. Assim, deverás estar preparada para que os juízes (ou os polícias) não dêem razão às ativistas e não vão colaborar. Isso seria contra o ADN destas instituições. Contudo, durante as ações, as detenções, o processo no tribunal, no prisão e depois do prisão, as ativistas podem criar uma pressão social com as suas atitudes e as suas palavras, que vão culminar em que um juiz não consegue continuar a cooperar com o sistema em que está inserido. Se tal acontecer, poderá criar um efeito de domino na própria instituição. O *status quo* – ou seja, o que é considerado normal e expectável – vai ter de mudar. Para isso, as pessoas comuns que têm funções essenciais no sistema vão ter de deixar de colaborar com o sistema.

Mas não te esqueças: estamos a falar dum pilar de apoio do sistema que nos trouxe até aqui, esta mudança vai demorar.

O Apoio Legal neste contexto é mais um **apoio *cognitivo* e *emocional* do que um apoio *técnico***.

Em primeiro lugar, precisamos de normalizar uma mentalidade que se livra do medo para estarmos em pé, com o orgulho de estarmos no lado certo da história e de termos uma estratégia para travar o colapso civilizacional. O sistema corrupto vai tentar, desde antes da ação até depois da sentença, fazer-nos sentir sozinhas e sem poder. **A luta principal no Apoio Legal será uma luta entre medo e coragem**.

Em segundo lugar, apresar dos formalismos dos tribunais, as interpretações importam. Os países que chamamos democráticos e os países que chamamos anti-democráticos têm essencialmente o mesmo Código Penal em efeito. O que a sociedade como um todo considera aceitável o os atitudes dos governos e instituições condicionam o que as leis podem significar no tribunal. Se ganharmos, não será porque algo está escrito de certa forma num certo papel. Se ganharmos, será porque a nova configuração do poder social implicará uma certa interpretação do caso. Por isso, **dizer a verdade com coragem, calma e abertura será a nossa arma principal ao longo do processo**.

Este manual vai dar-te algumas ferramentas pessoais, organizacionais e técnicas para acompanhar-te neste caminho.

[Download aqui](/recursos/manual-legal-v7.pdf)

{{< embed-pdf url="/recursos/manual-legal-v7.pdf" align="center" >}}

---

## Suplemento - Enquandramento Legal e Possíveis Acusações

Este documento acompanha o **Manual de Apoio Legal v.7** e deve ser lido de acordo com as recomendações nele.

Apresentamos as situações mais extremas e sanções máximas. No entanto, existe todo um sistema de agravantes e de atenuantes, e todo o um contexto social e político que funciona em paralelo com a aplicação da justiça. Esta lista pode ser incompleta e pode ficar desatualizada. (Tem em atenção que as consequências se agravam no caso da pessoa ter histórico.)

Esta é uma compilação das **leis que vimos serem utilizadas nas acusações pelo mundo contra as ativistas**. Reconhecemos que muitas delas nunca foram utilizadas contra as ativistas pela justiça social e climática em Portugal.

**O nosso entendimento é que o movimento pela justiça climática coloca no centro o cuidado do planeta e das pessoas, o que não garante que estas leis nunca sejam utilizadas pelo Estado como ferramentas de intimidação e dissuação.**

[Download aqui](/recursos/Suplemento-Enquadramento-legal-v1.pdf)


{{< embed-pdf url="/recursos/Suplemento-Enquadramento-legal-v1.pdf" align="center" >}}

---

## Legal Support Guide

*The translation to English was generously offered by Robin Boardman and you can find an online variant [here](https://robinboardman.com/civil-disobedience-a-legal-guide-for-activists-in-portugal/). Robin reorganized the information for digital readability and highlighted information relevant for foreigners and migrants. This file sticks to the Portuguese original to facilitate crosschecking.*

[Download here - Legal Support Guide](/recursos/Legal-Support-Guide-v7.pdf)

{{< embed-pdf url="/recursos/Legal-Support-Guide-v7.pdf" align="center" >}}

[Download here - Legal Framework and Possible Charges in Portugal](recursos/Supplement-Legal-Framework-and-Possible-Charges-in-Portugal-1.pdf)

{{< embed-pdf url="Supplement-Legal-Framework-and-Possible-Charges-in-Portugal-1.pdf" align="center">}}

---

## Versões Anteriores

Estes documentos servem para dar algum apoio e dicas às pessoas que possam querer participar em acções de desobediência civil.

Queremos oferecer toda a informação que podemos dar, mas isto também depende da tua colaboração e por isso pedimos-te atenção durante este processo. Não podemos garantir que todas as nossas respostas para todos os casos sejam “à prova d’água” porque a repressão nem sempre é previsível e depende das tácticas das forças repressivas.

Estes documentos não devem inibir-te de fazer algo. Pelo contrário, preparámo-los para te motivar e para te ajudar a encontrares a tua melhor forma de envolvimento. Temos que dizer desde logo que, muitas vezes, a polícia e a segurança não conhecem o enquadramento legal ou escolhem ignorá-lo.

A nossa prioridade é jamais deixar alguém para trás.

---

### Manual de Apoio Legal - Geral

Este manual, preparado pelo Climáximo, apresenta três partes principais:
- Conselhos gerais
- Enquadramento legal
- Possibilidade de detenção

Este documento serve como documento de apoio para a maioria das acções directas não-violentas que os movimentos sociais têm organizado.

[Manual de Apoio Legal v5](/recursos/Legal-brief-v5-1.pdf)

### Manual Legal de Desobediência Civil

Este manual, pelo Climáximo e utilizado na acção Em Chamas, está organizado em formato de pergunta-resposta e explica duma forma clara como proceder num caso de identificação ou detenção.

Este documento pode ser utilizado sozinho mas aconselhamos a leitura complementar do manual de apoio legal acima.

[Manual Legal de Desobediência Civil](/recursos/Manual-Legal-de-Desobediência-Civil.pdf)
