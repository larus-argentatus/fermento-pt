+++
title = "BigBlueButton - Technical Introduction for Facilitators"
date = "2023-07-26"
author = "Fermento"
type = "posts"
featured_image = "/images/recursos/guiao-technical-intro-bbb.png"
recursos = "guiões"
weight = "5"
+++

The PDF version of this page is available [here](/recursos/BBB-technical-intro-v1.pdf). If you download it, pay attention to the version number as we might update the files later.

This is a meeting facilitator’s guide for online meetings using the Big Blue Button platform. We use the below steps in the first ten minutes when people are still joining the meeting, to introduce the main tools and functionalities of the platform.

These steps are easily adaptable to Jitsi or Zoom meetings with small adjustments.

---

## Step 1: Welcome

At the starting time, say Welcome to all the participants. Introduce yourself, the meeting and existing roles.

*“Hi everyone! Welcome to the webinar X. My name is Ann, I am from the C collective and I will be facilitating this session. With us today are also Ben and Cecile who will be contributing for the discussion. A warm welcome to everyone in the room.” etc.*

## Step 2: BigBlueButton

e.g. *“We will be using BigBlueButton for this meeting.""*

*“BigBlueButton is a secure platform that does not collect your data. It is not end-to-end encrypted, as this technology does not exist in a robust form for group conferences at the moment."*

*“Another advantage of BigBlueButton is that it is device-neutral, as you can join the call from any internet browser, without installing an application.”*

## Step 3: Mic check

Ask people to mute their microphones when they are not talking. Plus, mute people who are causing background noise.

Check if everyone is connected by microphone. While entering the room, some people make the mistake of entering only by audio and therefore cannot talk.

e.g. *“To increase the sound quality and reduce network problems, we will ask you to mute your microphones. Under the slider, you can see the microphone symbol and by clicking it you can mute your microphone. When you want to talk, you can simply click the same button to unmute yourselves."*

*“I see that some people do not have their microphones connected. If this was a deliberate choice, there is no problem. But if you would like to be able to talk during this meeting, you will need your microphone. At the beginning, when you enter the link, the platform asks you if you wanted to enter by audio or by microphone. You probably have chosen audio. This means that the platform does not have access to the hardware itself. What you can do is that you can leave the meeting and return following the same link, this time entering with the microphone in order to allow the BigBlueButton servers connecting to the microphone. After entering, you can always mute yourselves.”*

## Step 4: Slider

Introduce the slider and explain that there will be slides.

If there will be parts without slides, you can also inform people about how to minimize the slider.

## Step 5: Welcome

We repeat Step 1 here, as more people probably entered in the meantime.

## Step 6: Public chat

Introduce the Public Chat section on the left column.

To make sure everyone saw it, ask people to write something on the chat. It can be an introduction or a simple Hello.

Some people use this moment to introduce reaction symbols like

```
++ (meaning: I agree),

?? (meaning: I have a question/doubt.) ,

* (meaning: I would like to talk.),
```

In this case, you can ask people to “agree” with these chat reactions by putting a `++` on the Public Chat.

Announce if you expect people to be active in the chat or if you would prefer them to use it in a moderated way.

## Step 7: Shared Notes

Introduce the Shared Notes section on the left column.

To make sure everyone saw it, ask people to write something on the notes. Some people use this moment to have a list of participants in the meeting, where everyone writes their name, collective, etc.

## Step 8: Mic check.

We repeat Step 3 here, as more people probably entered in the meantime.

## Step 9: Polls

Introduce the Polls functionality.

Do a test poll to make sure everyone sees the poll on the lower right corner of the screen. When everyone voted, publish the results and confirm that everyone sees the results on the lower right corner of the slider.

Some people use this moment to get more information about the participants, with a poll about regions, about ages, etc.

Some people use this moment as icebreaker, with a silly and/or provocative poll question.

## Step N: Breakout Rooms

We introduce breakout rooms only at the moment we need them.

Be aware that it is the most confusing part and try to be as clear as possible about what people should expect.

*“We will now leave this plenary session and enter into small breakout rooms. In a short while, the technical support team will launch the breakout rooms and you will see a screen where you are invited to a different room. You should accept the invitation and then you will be directed to a room similar to this one, but with less people. The platform will ask for permission for microphone again, so you should accept that and do the echo test. Then you will have T minutes in that small group. In the breakout rooms you will have an opportunity to discuss/share X. Any questions?"*

*“You can always see how much time is left on the top of the screen. At the end of that period, you will be automatically directed to this main room, and you will need to enter the room, do the echo test, etc. as you did in the beginning. The tech support team will always be available to accompany whatever help you might need. Any questions?”*

**Launch the breakout rooms only after making sure everyone understood what was happening.**
