+++
title = "SMAAARRT Objectives for Activists"
date = "2023-07-26"
author = "Fermento"
type = "posts"
featured_image = "/images/recursos/guiao-smaaarrt-objectives.png"
recursos = ["guiões", "atividades"]
weight = "4"
+++

This is a standard tool for setting goals for activities and campaigns. It is widely used in corporations, in NGOs, and in public institutions. We will tilt the interpretations towards social movements and give examples related to activism. This document was prepared by Climáximo.

Here is the [PDF document of this text](/recursos/SMAAARRT-objectives-guidelines-v2.pdf). [Here](/recursos/checklists/smaaarrt-objectives) you can find a simple form that can be used to set objectives and assess actions.

## Why objectives?

The learning cycle of an organization passes through recognizing failure and success[^1], identifying internal and external causes of failure and success[^2], making decisions for improvement, and take collective steps in order to avoid repeating the same mistakes or errors and in order to consciously repeat accurate and effective behaviors.

For this to actually work, the success criteria must be determined before taking action.

## How objectives?

We strongly recommend the goal-setting to be done 1) with the entire action team, 2) in a calm environment, 3) several days before the activity.

Everyone has an expectation from an action. Listening to the expectations of the others, people can adjust their own expectations as well as their behavior[^3]. They can also do a reality-check by hearing the reflections of the others.[^4]

This is also a team building activity, and the conversation itself can actually improve your chances of success as activists grow a better understanding of their state.[^5]

Finally, objectives must be set in advance because the activity itself, with its surprises, disappointments, frustrations and excitements, will blur the vision afterwards. It is essential that the team has collectively-agreed-upon objectives so that the learning process is an organizational process. This cannot be achieved after the action with success criteria and success evaluation all mixed up in people’s minds.

## What objectives?

You would need various kinds of objectives:

- **Process (P) objectives**: how did the preparations work? shared burden? participation? internal democracy? involving other groups?
- **Execution (E) objectives**: how did the action look? interaction with passers-by or security? clear roles and tasks? duration? the real-time effect of the action? safety of the activists?
- **Social impact (I) objectives**: media coverage? social networks? reactions of other organizations? reactions of decision-makers?
- **Organizational (O) objectives**: increased visibility of the organization? recruitment? capacity building?


All of your objectives should be SMAAARRT, that is

- **S**pecific
- **M**easurable
- **A**mbitious
- **A**chievable
- **A**ctionable
- **R**ealistic
- **R**elevant
- **T**imed

---

|Not Specific|Specific|
|---|---|
| --- | --- |
|We will give visibility to the precarious working conditions. (I) | We will *produce a visual image* of the *psychological impacts* of the *short-term contracts*.|
| --- | --- |
|We will denounce this corporate conference. (E) |	We will *interrupt* the conference for ten minutes. |
| --- | --- |
| We will raise awareness on bad climate policies. (I) | 	There will be *at leastone news item* about our action *against the new airport project* and its incoherence with climate science. |
| --- | --- |
| We will protest against the planned oil drills. (I) |	We will *highlight the role of the Ministry of Environment* in the decision of the Environmental Protection Agency.|
| --- | --- |
| This action will put our organization at the center of the coal discussions. (O) |	In the following three months, when there are new developments about coal, *journalists will call us to ask for opinion*.|
|---|---|

---

|Not Measurable|Measurable|
|---|---|
| --- | --- |
|We will do a mass protest. (E)| We will mobilize at *least 3000 people*, of which *1000 in Lisbon*, in a total of *at least 15 cities*.|
| --- | --- |
|Our action will appear in mainstream media. (I)|	*At least 7 of the main journals* will cover our action. We will appear on the news on *one television channel*. |
| --- | --- |
|Our action video will go viral on Facebook. (I)| Our action video will have *at least 7000 reach*, *at least 1000 views*, and *at least 80 shares*. |
| --- | --- |
|The preparations of the demonstration will be a democratic process. (P)|	We will organize *at least four publicly announced preparation meetings*.|
|---|---|

---

| Not Ambitious|Ambitious|
|---|---|
|---|---|
| We will organize a die-in on the pavement. (E)| We will block a *main venue*.|
|---|---|
| Our event will attract media attention. (I)| We will have *journalists participating in our event*.|
|---|---|
| We will do presentations to call for action. (O)| We will *coach at least 5 new speakers* through the presentations.|
|---|---|
| With our action, we will tell them that we disagree with their policies. (E) | We will *interrupt the meeting for 30 minutes*, not letting them continue their business-as-usual.|
|---|---|

---

| Not Achievable | Achievable |
|---|---|
|---|---|
| After this protest, the decree law favoring fossil fuel extraction will be revoked. (I) | The *Prime Minister will publicly address our action* and take a stand on the decree law. |
|---|---|
| We will bring world peace with this mass demonstration. (I) | Due to the mass demonstration at the same time as the plenary session, the *parliament will be forced to delay the voting on the invasion*. |
|---|---|
| Seven more local groups will appear. (O) | *Activists from at least 10 cities will contact us* to form local groups in their cities. |
|---|---|
| We will use consensus decision-making during this direct action. (P/E) | We considered various scenarios and agreed on the *levels of consent* we need to build at each case. |
|---|---|

---

| Not Actionable | Actionable |
|---|---|
|---|---|
| We will launch the campaign. (E) | We will *organize a flash-mob in a shopping mall* to launch the campaign. |
|---|---|
| We will write a report on just transition. (P) | We will involve at least 10 scientists and 10 unionists to write a joint report. |
|---|---|
| We will distribute flyers to raise awareness. (I) | We will distribute flyers that *invite people to join the protest next week*, and we will *have a stand for people who may want to ask questions*. |
|---|---|
| We will produce three banners. (P) | We will *organize openly announced material preparation sessions* to paint three banners. |
|---|---|

---

| Not Realistic | Realistic |
|---|---|
|---|---|
| Tomorrow, we will organize a rapid response action with ten thousand people and block a main avenue. (E) | We *call for a rapid-response open assembly tomorrow* to prepare a mass blockade. |
|---|---|
| We will have a documentary of this event. (P) | We will *form a media team* that will set its goals *based on their capacities*. |
|---|---|
| At least 1000 people will camp in the main square. (E) | *If it’s not raining and if there are no police barricades*, at least 1000 people will camp in the main square. |
|---|---|
| We will involve 100 people in the preparations. (P) | We will *create autonomous working groups* for external communications, logistics, buses and internal communications, each of which will identify their necessities. |
|---|---|

---

| Not Relevant | Relevant |
|---|---|
|---|---|
| We will organize a protest against oil extraction. (I) | We will organize a protest against oil extraction in the *World Cup financed by this oil corporation*. |
|---|---|
| We will organize a solidarity action with the car factory workers. (O) | We will *mobilize our existing networks* of labor rights organizations to support a joint protest within our *just transition campaign*. |
|---|---|
| We will launch a video of the blockade of the conference. (I) | We will launch a video of the blockade of the conference *while the conference is still continuing*. |
|---|---|
| We will organize an introductory meeting. (O) | *After a mass protest*, we will organize an introductory meeting for interested people. |
|---|---|


---

| Not Timed | Timed |
|---|---|
|---|---|
| We will send a press release and edit a video of the action. (E) | We will send a press release *while the action is still ongoing*, and we will release a video in social media on the *next day afternoon*. |
|---|---|
| We will organize preparatory meetings. (P) | We will organize *biweekly meetings* until the last month. In the last month before the conference we will have *weekly* meetings. |
|---|---|
| We will organize a creative flash-mob. (I) | In order to catch the media attention of the general strike to release a *video on the day of the strike*. |
|---|---|
| We will have a learning cycle. (P) | *One week before* the action we will have a meeting to finalize objectives, *the day after* the action we will have a debriefing, *one week after* the action we will have an evaluation meeting. We will then *share main learning points with our organization* in a document prepared in that meeting. |
|---|---|


Here you can find a simple form that can be used to set objectives and assess actions: [SMAAARRT objectives checklist v2](/recursos/SMAAARRT-objectives-checklist-v2.pdf)

[^1]:Particularly in the civil society organizations which rely on the motivation of the volunteers, we see a chronic success narrative. Many of these organizations have only declared victories for the last thirty years, but somehow the world is not much better. Not acknowledging failures and not addressing them thoroughly causes blindness towards mistakes, errors and structural weaknesses, which in turn creates a conformist organizational culture surrounded by *destructive cheerfulness*.
[^2]:Even when organizations recognize a success, they often fail to detect why that activity was successful and exactly what attitudes or decisions should be repeated.
[^3]:Maybe a valid expectation of one activist depends on the way you fulfill your role (maybe you just needed to chip in a smile?)…
[^4]:If someone says “Well, I’d love to have huge media attention, but with the World Cup Semi-Finals starting at the moment we send the press release, I don’t really expect that.”, another may realize “Oh, I completely forgot about the World Cup calendar!”.
[^5]:If someone says “Turns out my mother will have a surgery that day. I was really enthusiastic about this action but now my main concern is that we finish on time so I can go to the hospital.” you may intentionally increase your discipline and efficiency to accommodate this need.
