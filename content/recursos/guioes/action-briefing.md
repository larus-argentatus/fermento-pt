+++
title = "How to present an Action in an Action Briefing"
date = "2023-07-26"
author = "Fermento"
type = "posts"
featured_image = "/images/recursos/guiao-action-briefing.png"
recursos = "guiões"
weight = "2"
+++

The PDF version of this manual, prepared by Climáximo, is available [here](/recursos/Action-briefing-v1.pdf). If you download it, pay attention to the version number as we might update the files later.

---

## Action Briefing

Some actions are organized over several meetings with the participation of all the activists involved, and some actions are organized by an affinity group while other activists only show up to take part in the action. This note for organizers applies to the latter case.

Many people show up in an action briefing with some doubts, because they didn’t participate in the process of its creation. Other people may have doubts because they may not feel sufficiently informed about the issue. So, what we as organizers need to do is not only to explain the action, but also contextualize the action and motivate people to participate in it.

Here is one way we frame actions in those meetings.

1. **Context**: We first explain the political context in which the action happens. Depending on who is there, you may start with climate urgency or carbon budget. Or maybe there is an auction or voting coming up about a particular project.

Basically, we start by explaining ***why we must act now***.

We do not open a discussion about this point.

2. **Concept**: So, something bad is happening and we must respond. ***What is the message we want to transmit***? This is the second step of the briefing. We try to express it as succinctly as possible, hopefully with a single slogan. In a way, this would be the summary of the press release.

We do not open a discussion about this point.

3. **Image**: Now, what is the action that would transmit that message? We typically think in terms of image. This is defined by the concept, but determined by our capacity (resources, logistics, availability, media outreach etc.).

It’s maybe hundreds of people putting their bodies in front of a coal excavator (like in Ende Gelände), or maybe a group of animals invading a corporation’s office (like in EZLN actions during Climate Games).

We discuss this point a little bit, because there may be interesting supplementary proposals or there may be substantial objections to the proposal.

4. **Details**: Then we talk about turning it operational. This is open discussion. We discuss about

- Preparations: materials, trainings, meetings, meeting point and time, etc.
- Action roles: activists, mediators, filming, communicators (for media and for passers-by), back-office, etc.
- Communication: press release, video, live-stream, etc.
- Debrief: when and where we would make an evaluation.

We start with the context (motivate), continue with the concept (inspire), then demonstrate the image (amaze), and finalize with the details (execute).

But perhaps this is still too abstract.

Below are some examples.

## GALP Energia vs EZLN – Exército Zoológico de Libertação da Natureza

{{< vimeo 218201229 >}}

**Context**

- GALP/ENI consorcium wants to start offshore drilling for oil and gas next week. The Portuguese government talks a lot about carbon neutrality, but it gave permission for this project. This would be the first offshore oil drill in Portugal. The fossil fuel reserves already tapped and being extracted are more than our carbon budget, **this drill must be stopped now**.

**Concept**

- Portugal has a strong cultural connection with the oceans. The extractivist GALP/ENI threatens to attack our ocean and our climate. We will fight back. **We are nature defending itself!**

**Image**

- We will **dress up as sea animals and invade GALP headquarters**. We will bring the ocean with us. We will make noise and a mess, although uncomparably less than what GALP/ENI would do to our oceans. We will stay for a couple of minutes, then leave, make our dance, and go away.

**Details**

- Costumes, filming, choreography, banners, video, press release, trainings, meetings, etc.

## EDP (Ende Gelaende, Lisbon)

{{< vimeo 244485620 >}}


**Context**

- A few days before COP-23, thousands of activists will occupy one of the biggest open air lignite mines in Europe. The governments have been negotiating for 23 years, and emissions keep on going up. Ende Gelaende will show how to actually cut emissions: by stopping the fossil fuel industry.While in Portugal, EDP has the coal power plant in Sines, responsible for 10% of national emissions. EDP does a lot of greenwashing talking about electric cars (coal cars?) and a lot of whitewashing too – with its recently opened art museum. We **should show our support to our comrades in Ende Gelaende**, and we need to **bring coal into climate agenda in Portugal**.

**Concept**

- We will go to the museums of EDP and draw the red lines for a liveable planet. **We will tell EDP that coal belongs to museums** and not to energy production any more.

**Image**

- We will draw our red lines around Museu de Eletricidade (old power plant, now electricity museum), because fossil fuels should stay inside the museums. Then we will literally **knit and weave red lines at the entrance of the newly opened art museum** (MAAT).

**Details**

- Knitting material, banners, photos, video, press release, flyers to distribute.

## Marcha pela Ciência & Marcha pelo Clima

{{< youtube _HXNZs5a2IE >}}

**Context**

- Trump’s policies triggered a massive movement all around the world defending evidence-based policies for climate and social justice. In April, **there will be global marches for science and (one week after) for climate**. It is important to underline that one doesn’t need to say what Trump openly says to do what Trump does. In fact, science in Portugal is underfunded and precarious, and while the government talks about carbon neutrality, it also allows new oil and gas projects. **Trump’s election is an opportunity for revealing the hypocrisy of all governments, and demand evidence-based climate policies**.

**Concept**

- Over the course of 10 days, we will have the opportunity to summarize the world and society we want. First, on April 22nd, we will **march for science, demanding more funding for those who set the factual base of evidence-based policies**: scientists, teachers, universities. One week after, on April 29th, we **march for climate justice. We demand sound policies for a liveable planet**, and more concretely the cancellation of new oil and gas contracts.This will be a bridge to the **May 1st labour demonstration**, where we will demand **jobs and justice**.

**Image**

- We will **bring people from all backgrounds and all priorities to the streets** throughout ten days: scientists, students, environmentalists, frontline populations of fossil fuel extraction, youth, and workers from all sectors.

---

These examples may help to visualize how to frame an action and how to structure an action briefing.

Of course, preparing an action briefing is perhaps the least difficult part of preparing an action. But if you need numbers or if you need to motivate people for confrontation, a well-planned and carefully designed action briefing may be a useful organizational tool.

Here, you can draft your action briefing:

{{< figure src="/images/recursos/action-briefing-table.png" align="center" width="100%" >}}
