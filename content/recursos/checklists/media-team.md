+++
title = "Checklist - Setting up a Media Team"
date = "2023-07-26"
author = "Fermento"
type = "posts"
featured_image = "/images/recursos/checklist-media-team.png"
recursos = "checklists"
weight = "9"
+++

---

This document applies to direct actions, conferences, public sessions, mass demonstrations and action camps. At the end of the checklist, we give a few examples of how teams may be organized.

The list applies to both the documentation team (media produced _by the organizers_) and press work (media produced _by others_). Although these two are quite different tasks, in grassroots organizations these two teams either merge or work very closely, in order to guarantee coherence in messaging or simply due to limited resources.

We avoid stating rules and principles, for two main reasons. Firstly, the expansion and diversification of social networks and the rapid changes in their algorithms make all rules out of date within a time frame of months. Secondly, the capacities of the group, the goals of the action and the media landscape condition the messaging decisions. Therefore, we generally limit ourselves to questions (rather than give definite answers) and to tools to build answers collectively.

## Planning

- [ ] Define media objectives (journal publications, TV coverage, reach, views, shares, number of videos and photos, etc.). Write them down;
- [ ] Define key messaging points and write the narrative guidelines;
- [ ] Decide on social network needs of the action;
- [ ] Study the algorithms of the relevant social networks;
- [ ] Identify the teams and the number of people necessary in each;
- [ ] Write down a detailed editorial calendar. Identify which team should do exactly which task on which day and at what hour. You can use teamup.com to create online collaborative calendars.
- [ ] Share media objectives, key messaging points and the narrative with the rest of the organizers. Receive feedback and update the documents.

### Media teams

Here are some media teams you may want to set up.

- **Press team:** Writes and sends press releases. Organizes press conferences. Organizes media hours (a guided tour through the event, designed for journalists);
- **Social networks:** Publishes posts and stories in the social network accounts, like Facebook, Instagram or Twitter. Each social network requires different kind of content and a different language, works with different timings and has a different audience. Either have different people taking care of different social network accounts, or downgrade your social media presence to the basics;
- **Photo team:** Takes photos during the action. Selects a limited amount of photos. Edits and improves the photos. Delivers to the documentation team;
- **Video team:** Films key moments of the action, either in parts or – if extremely important – fully. May need to do live coverage. Edits the videos. Delivers to the documentation team;
- **Interview team**: Interviews the participants, organizers and the special guests, preferably in video;
- **Documentation team:** Receives from the photo team and the video team; compiles and synthesizes the story. Produces summary texts;


## Preparation

- [ ] Get access to existing social network accounts;
- [ ] Create the missing social network accounts and define a strategy to build up their visibility;
- [ ] Recruit volunteers (or professionals, if that’s the case) for the media team;
- [ ] Make sure that the team members know not only the action itself, but also the background and the context;
- [ ] Run a sanity check on the editorial calendar, presence in social networks, and the media objectives. Do you have enough capacities to achieve the objectives? Do the team members agree with the editorial calendar, do they have further suggestions?
- [ ] Prepare a language agreement with the media team. Define the tone and phrasing, hashtags and slogans, and a communication policy (if and how to answer comments, gendered language etc.);

## Before the Action​


- [ ] Write up the mandates and responsibilities of each team member. Please write this up and spend at least 30 minutes of collective time looking at it, to see if there is anything unclear or missing;
- [ ] Share access to social network accounts, email accounts and the website(s);
- [ ] Agree on the need of meeting time during the action (recommended), on its scheduled moments, its the location; and insert it on the collaborative calendar;

## During the Action​

- [ ] Send reminders to team members 30 minutes to 1 hour before their tasks are due to start;
- [ ] Track the beginning of any team’s activity throughout the whole action;
- [ ] Review the contents you are producing, especially in the first moments (as the action gets going, you’ll all be more aligned);
- [ ] Don’t forget to always provide specific feedback if something is not up to expectations (feedback should be given to an action and not to a person’s whole personality) and to congratulate people on a job well done;
- [ ] Listen to your team’s concerns and, if needed, adjust the course of action for a better outcome;
- [ ] In the end of the action, take time to thank in person everyone involved, if possible by holding a brief last all-person meeting;

### *In case you scheduled meetings*:

- [ ] Review the [Checklist for preparing a meeting](/recursos/preparing-meeting);
- [ ] Get there on time, start and close on time;
- [ ] Explicitly save space in the meeting to let everyone speak their minds, how they are feeling and how can our collective work be further improved. (You can make this by, for instance, going round the table with the answer to these questions, while letting them now they are not obliged to speak and can pass on to the next person);
- [ ] Review the job done;
- [ ] Review the tasks and responsibilities of everyone until the next meeting;
- [ ] End on a positive note;

## After the Action

- [ ] Keep tracking any tasks yet to complete (posting after-movies of the action, press release of how it went, etc);
- [ ] E-mail or message a thank you note to everyone involved (from team members to media partners or even just the person who borrowed you a camera for the action);
- [ ] (in case the team is supposed to dissolve after the action) Ensure next steps for the organization you are communicating are taken care of (e.g. if you need to transition social media accounts to another person who would do the work from that day on).

## End of the Checklist

*See examples in the following.*


---

### Example 1: EZLN vs. GALP action

{{< figure src="https://arquivo.climaximo.pt/wp-content/uploads/2017/05/10-1024x641.png" align="center" width="100%" height="auto" >}}

*[Link](http://www.climaximo.pt/2017/05/19/ezln-vs-galp-english/)*

This direct action involved a team of 30 people in total. Dressed as sea animals, activists entered the headquarters of GALP Energia. No media team was formed separately. Media work was integrated in the action preparations as a whole. More specifically,

- The press release was written collectively;
- One person was responsible for selecting and improving photos, and then sending out the press release and publishing social media posts with those photos;
- During the action, one person took photos and three persons filmed;
- After the action, one person edited the video;

The type of the action (the surprise element for media, and affinity group organizing rather than publicizing openly) limited the press work. The media team did not have a separate debriefing.

---

### Example 2: Rise for Climate (8 September 2018) demonstration

{{< figure src="https://arquivo.climaximo.pt/wp-content/uploads/2018/09/sign05-1024x683.jpg" align="center" width="100%" height="auto" >}}
*[Link](https://arquivo.climaximo.pt/2018/09/09/wrap-up-marcha-mundial-do-clima-2018-2/)*

This mass protest was organized in three cities: Lisbon (800 people), Porto (200 people), Faro (200 people). Separate media teams were formed for each city, who reported back to action coordination. More specifically,

Photo team was 2-5 people per city. In Lisbon, the organizers had another task force of 4 people who distributed tiny flyers to participants, explaining the hashtags and where to send their photos. One person made a selection of 40 photos for the entire event (from the 2000 received), another person improved them.

Video team was 1-3 people per city. One person received all the videos filmed, and edited the action video.

There was no interview team nor social media team.

The documentation team was a single person who wrote and sent out the wrap-up text.

The total amount of volunteers in the media team would add up to 10-12 activists, some with repeated roles.

---

### Example 3: Rebellion Week (14-21 April 2019) action week


{{< figure src="https://arquivo.climaximo.pt/wp-content/uploads/2020/08/14-1024x587.png" align="center" width="100%" height="auto" >}}

*[Link](https://arquivo.climaximo.pt/2019/04/24/wrap-up-the-rebellion-week-in-portugal-in-10-acts/)*

This was an action week consisting of 10 different actions. Each action was organized by an affinity groups. While some activists did belong to more than one affinity group, the actions were independent of each other. Each affinity group had its own media team, who then reported to the media team coordination. More specifically,

- Many affinity groups did live streaming through social media;
- Others only took photos;
- Some edited action videos;
- All were encouraged to write a press release collectively, which was sent out by the coordination during or after the action (either using the links to live videos or with photos received);
- The media team coordination served as the documentation team which compiled the actions and produced a coherent narrative for the entire week;
- Each affinity group had its own media contact person to explain the action, while the media team coordination only talked about the week as a whole;
- Each affinity group had small media teams consisting of 3-5 people (1-2 to film, 1-2 to take photos, 1 media contact person). The media team coordination was another 2-3 people throughout the week;

---

### Example 4: Camp-in-Gás action camp against fossil gas and for climate justice

{{< figure src="https://arquivo.climaximo.pt/wp-content/uploads/2019/07/IMG_9248.jpg" align="center" width="100%" height="auto" >}}

*[Link](https://arquivo.climaximo.pt/2019/08/01/wrap-up-camp-in-gas/)*

This was a 5-day action camp with 200 participants, that had workshops and trainings and ended with a mass action of 400 people. There were two separate media teams: before the camp, and during the camp.

The media team before the camp consisted of 5-6 people. They prepared an editorial calendar to feed social media accounts and send out press releases.

The media team during the camp consisted of 21 activists and 1 coordinator. There was a clearly timed editorial calendar with hourly tasks per person. The media team had daily meetings during the camp to give feedback and adjust the plans. More specifically,

- Press team consisted of 3 people: 1 person sent out press releases already written, 1 person contacted local media, 1 person organized media hours. This team was independent of the rest;
- Social networks team was formed by 6 people who published stories. Their schedule was independent of the rest, as other people took high quality images for editing;
- Photo team, consisting of 4 people, took photos during the workshops and the actions. One person then selected 3-4 photos per day, improved them and delivered to the documentation team;
- Video team, consisting of 4 people, filmed the workshops and the actions. Two people edited a total of three videos (special plenary session, the action, the camp);
- The photo team and the video team split into different parts of the terrain during the action. Throughout the camp, hashtags were announced regularly to compile images taken by individuals;
- The interview team consisted of 4 people who had specific questions at hand. They interviewed selected participants and delivered the results to the video team for the camp video;
- The documentation team was only one person working closely with other teams, who wrote down daily summaries;
