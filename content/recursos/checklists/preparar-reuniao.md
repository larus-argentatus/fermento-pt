+++
title = "Checklist - Preparar uma Reunião"
date = "2023-07-26"
author = "Fermento"
type = "posts"
featured_image = "/images/recursos/checklist-preparar-reuniao.png"
recursos = "checklists"
weight = "1"
+++

Aviso: Organizar é um acto político. Uma reunião não pode ser reduzida a uma checklist. Contudo, esta lista pode ajudar com a logística e talvez servir de lembrete para algumas componentes chave para organizar a reunião.


---

## Antes da Reunião

- [ ] A reunião é anunciada, o que significa que é publicitada ( através do website do grupo, newsletter, página de Facebook, etc…) ou que os membros do grupo são informados (através da mailing list, emails individuais, telefone, SMS, WhatsApp/Telegram/grupo Signal, etc…);
- [ ] O local da reunião está confirmado.
- [ ] Temos tudo o que é necessário para a reunião.
  - [ ] Projector.
  - [ ] Ecrã para projecção.
  - [ ] Sistema de som.
  - [ ] Computador.
  - [ ] Ligação Internet.
  - [ ] Quadro branco.
  - [ ] Marcadores para quadro.
  - [ ] Bloco de flipchart.
  - [ ] Papéis.
- [ ] Sei quantas pessoas esperar.
- [ ] Li as actas da reunião anterior para assuntos e tarefas pendentes, feedbacks necessários e próximos eventos e tarefas.
- [ ] Preparei uma agenda de reunião provisória.
- [ ] Sei quem irá facilitar a reunião.
- [ ] Os facilitadores desenvolveram ferramentas para facilitar processo de decisão colectivo.
- [ ] Os participantes  conhecem a agenda da reunião ou pelo menos os objectivos da reunião.
- [ ] Conheço a atitude/política/decisão  do grupo acerca da moderação progressiva.
- [ ] Sei quando a reunião deve acabar.

---

## Durante a Reunião

- [ ] Os participantes conhecem-se uns aos outros ou pelo menos os organizadores da reunião.
- [ ] Os participantes conhecem os sinais de mão que podem ser usados durante a reunião.
- [ ] A agenda da reunião é anunciada.
- [ ] Há um facilitador (quer decidido no momento, ou anunciado se previamente decidido).
- [ ] Todos devem saber quando a reunião deve acabar.
- [ ] Actas de reunião estão a ser escritas.
- [ ] Garantimos que toda a gente participa (Moderação progressiva? Recém-chegados?)
- [ ] Cronometramos os tópicos  cuidadosamente de forma a não apressar assuntos potencialmente importantes.
- [ ] Certificamo-nos que as tarefas são entregues a pessoas.
- [ ] Garantimos que as tarefas tenham prazos de conclusão.
- [ ] Sabemos quando e onde a próxima reunião será – caso exista.

---

## Após a Reunião

- [ ] Limpamos o espaço de reunião.
- [ ] Devolvemos qualquer material emprestado.
- [ ] As actas da reunião e decisões são partilhadas com o grupo/organizadores.
