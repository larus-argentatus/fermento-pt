+++
title = "Checklist - Writing Meeting Minutes"
date = "2023-07-26"
author = "Fermento"
type = "posts"
featured_image = "/images/recursos/checklist-writing-meeting-minutes.png"
recursos = "checklists"
weight = "8"
+++



## Before the meeting

- [ ] I know the topics and agenda of the meeting;
- [ ] I know how detailed the meeting minutes are expected to be: just the decisions and information, or all the discussions? (In the latter case, it may be better to have more than one person taking notes.);
- [ ] I know if there is any validation process: if the notes are to be sent to approval or modifications;
- [ ] I am aware of the deadline to send the minutes;
- [ ] I know where to send the minutes to;


## During the meeting

- [ ] I took note of the attendance list (if considered necessary);
- [ ] I structure the notes as topics and subtopics;
- [ ] I interrupt the discussion if it is too quick for me to take notes;
- [ ] For the cases in which the reader is supposed to act, I use ACT (bold and in capitals) followed by the description of the necessary action;

> *Example: “***ACT***: Everyone fill in the form [link] until next Monday.”*

- [ ] For specific tasks, I write the name of the responsible person in bold.

> *Example: “On Tuesday we will have an action training.* ***Ernesto*** *will prepare the content.”*


## After the meeting

- [ ] I formatted and organized the notes so that a person who was not in the meeting can understand the summary;
- [ ] At the very beginning of the minutes I created a section, KEY, where I compiled the to-do lists by person;

> *Example:*
> ***“KEY***
>
> ***Vladimir**: write press release proposal*
>
> ***Rosa**: prepare speech for Wednesday action, prints pamphlets*
>
> ***Ernesto**: prepare action training, schedule attendance for the clinic*
>
> *etc.”*

- [ ] I sent the meeting minutes, or started the validation process;
- [ ] If there is a validation process: I updated the document and sent the final version;
