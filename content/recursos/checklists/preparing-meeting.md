+++
title = "Checklist - Preparing a Meeting"
date = "2023-07-26"
author = "Fermento"
type = "posts"
featured_image = "/images/recursos/checklist-preparing-meeting.png"
recursos = "checklists"
weight = "7"
+++

Disclaimer: Organizing is a political act. A meeting cannot be reduced to a checklist. However, this list may help you with logistics and perhaps serve as a reminder for some key components of organizing the meeting.

---

## Before the meeting

- [ ] The meeting is announced, meaning either that it is publicized (through the group’s website, newsletter, Facebook page etc.) or that the members of the group are informed (through the mailing list, individual emails, phone, SMS, Whatsapp/Telegram/Signal group etc.);
- [ ] The meeting location is confirmed;
- [ ] We have everything we need for a meeting;
  - [ ] Projector;
  - [ ] Screen for projection;
  - [ ] Sound system;
  - [ ] Computer;
  - [ ] Internet connection;
  - [ ] Whiteboard;
  - [ ] Board markers;
  - [ ] Flip charts;
  - [ ] Papers;
- [ ] I know how many people to expect;
- [ ] I read the previous meeting minutes for pending topics and tasks, necessary feedbacks, and upcoming events/tasks;
- [ ] I prepared a tentative meeting agenda;
- [ ] I know who would facilitate the meeting;
- [ ] The facilitators developed tools to facilitate collective decision making;
- [ ] The participants know about the meeting agenda, or at least the objective of the meeting;
- [ ] I know the group’s attitude/policy/decision about progressive moderation;
- [ ] I know when the meeting should end;


## During the meeting

- [ ] Participants know each other or at least the organizers of the meeting;
- [ ] Participants know the hand signals that may be used during the meeting;
- [ ] The meeting agenda is announced;
- [ ] There is a facilitator (either decided upon at the moment, or announced if pre-decided);
- [ ] Everyone knows when the meeting should end;
- [ ] We are writing meeting minutes;
- [ ] We make sure everyone participates (Progressive moderation? Newcomers?);
- [ ] We time topics carefully in order not to rush potentially important subjects;
- [ ] We make sure people are assigned to tasks;
- [ ] We make sure deadlines are set for tasks;
- [ ] We know when and where the next meeting will be – if any;


## After the meeting

- [ ] We cleaned the meeting space;
- [ ] We delivered back any borrowed materials;
- [ ] The meeting minutes and decisions are shared with the group/organizers;
