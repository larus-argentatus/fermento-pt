+++
title = "Checklist - Fotografar em Ações"
date = "2023-07-26"
author = "Fermento"
type = "posts"
featured_image = "/images/recursos/checklist-fotografar-acoes.png"
recursos = "checklists"
weight = "3"
+++

Disclaimer: Fotografar em público (particularmente a polícia) pode ter consequências legais em alguns países. Esta lista visa ajudar-te com a logística e talvez servir como um lembrete para alguns componentes-chave da fotografia, mas não inclui questões legais.

---

## Antes da Ação

- [ ]  Sei quem mais está a tirar fotos.
- [ ]  Sei que tipo de fotos devo tirar (multidões, paisagem, faixas, cartazes, oradores, detalhes, confrontos, etc.).
- [ ]  Conheço o caminho.
- [ ]  Para fotos de acção: conheço locais elevados ao longo do caminho.
- [ ]  Para fotos de confrontos: conheço locais seguros com bons ângulos e boa luz.
- [ ]  Devo publicar as fotos directamente ou devo enviá-las a alguém?
- [ ]  Sei a quem devo enviar as fotos / onde publicar as fotos.
- [ ]  Sei quando devo enviar/publicar as fotos.
- [ ]  Se vais publicar as fotos: sei o(s) hashtag(s) da acção.
- [ ]  Tenho um crachá/t-shirt que me identifica enquanto fotógrafa(o) da organização.
- [ ]  Estou ciente das possíveis reacções policiais: Esperamos confrontos físicos? Devo tirar fotos de alguma faixa antes da polícia / segurança me tirar a câmara?
- [ ]  Estou a par das medidas de segurança: Há activistas que não devem aparecer em nenhuma parte das filmagens (para o caso da segurança me apreender a câmara)? Há activistas que definitivamente devem aparecer no vídeo (e.g. violência policial contra membros do parlamento, cidadãos séniores ou famílias)?
- [ ]  Configurei a minha câmara (considerando luminosidade, brilho, flash, ângulo/lentes).
- [ ]  A minha câmara tem bateria carregada. (Em caso de emissão em directo: tenho uma bateria extra.)
- [ ]  Sei quem estará a filmar a acção.
- [ ]  Falei com a equipa de filmagens e sei o que necessitam de minha parte (os fotógrafos podem, juntamente com as fotos, fazer pequenos vídeos de 3 a 5 segundos).

---

## Durante a Ação

- [ ] De que direcções vem a luz?
- [ ] Temos pelo menos cinco fotos em que a multidão e a faixa principal são filmados.
- [ ] Para fotos de muita gente/multidões: Tirei várias fotos com a faixa principal e os manifestantes atrás dela.
- [ ] Para fotos de muita gente/multidões: Tirei várias fotos onde se pode confirmar o número de manifestantes.
- [ ] Para fotos de paisagens: tirei várias fotos com várias faixas e cartazes.
- [ ] Para fotos de paisagens: as minhas fotos têm cartazes ou faixas que identificam a acção.
- [ ] Temos fotos de todas as faixas.
- [ ] Para fotos de faixas: todas as faixas são legíveis nas minhas fotos.
- [ ] Para fotos de faixas: As pessoas que seguram faixas nas minhas fotos não parecem distraídas ou cansadas (podem estar a gritar palavras de ordem ou simplesmente a sorrir para a câmara).
- [ ] Temos uma selecção de fotos com vários cartazes.
- [ ] Para fotos de cartazes: As pessoas que seguram cartazes nas minhas fotos não parecem distraídas ou cansadas (podem estar a gritar palavras de ordem ou simplesmente a sorrir para a câmara).
- [ ] Temos grandes planos de todos os oradores na manifestação ou conferência.
- [ ] Para fotos de oradores: As fotos não estão desfocadas.
- [ ] Para fotos de oradores: Tenho fotos que mostram várias emoções de um orador (raiva, alegria, determinação, etc.).
- [ ] Temos algumas fotos de detalhes: pessoas a falar umas com as outras, famílias, uma interacção com agentes da autoridade, pessoas com fatos coloridos ou t-shirts com slogans, pessoas a segurar cartazes com frases longas, celebridades, etc.
- [ ] Se houver uma largada de faixas: Tirei várias fotos horizontais e verticais que apanham a frase completa.
- [ ] Se houver confrontos: tirei fotos das faixas que a polícia apreendeu.
- [ ] Para fotos de confrontos: documentei a violência policial.
- [ ] Para fotos de confrontos: as minhas fotos incluem a mensagem política – e não apenas a confrontação física per se. (Por vezes um cartaz ou faixa está visível. Por vezes o edifício tem um logótipo. Por vezes os activistas têm t-shirts que os identificam com uma causa. Talvez algumas mensagens verbais por parte dos activistas. No pior caso, eu própria falo para descrever e contextualizar a situação.)
- [ ] Entrego todas as fotos urgentes imediatamente. (As razões para a urgência podem ser: tem de se enviar um comunicado de imprensa imediato. /A polícia apreendeu a minha câmara. / A hashtag está a tornar-se viral e é esperado que as organizadoras contribuam. )
- [ ] Se a equipa de filmagens solicitou alguma coisa: tenho material suficiente para o vídeo.

---

## Após a Reunião

- [ ] Sei onde está a minha câmara, e se e como a posso recuperar.
- [ ] Entrei em contacto com outros fotógrafos e confirmei que temos boas fotos de todos os tipos (multidão, paisagem, faixas, cartazes, orador, detalhes, confrontos, etc.).
- [ ] Seleccionei 10-20 fotos em boas condições e enviei-as aos organizadores separadamente.
- [ ] Enviei todas as fotos aos organizadores.
- [ ] Se é esperado que publiques: fiz uma selecção cuidadosa de 5-10 fotos que incluem multidões, faixas, slogans, oradores e detalhes; publiquei-os com um texto descritivo e o hashtag correcto.
- [ ] Se há um vídeo imediato a ser editado: enviei todas as fotos e vídeos curtos à pessoa que irá editar o vídeo.
