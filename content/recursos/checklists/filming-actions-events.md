+++
title = "Checklist - Filming in Actions & Events"
date = "2023-07-26"
author = "Fermento"
type = "posts"
featured_image = "/images/recursos/checklist-filming-actions.png"
recursos = "checklists"
weight = "11"
+++

Disclaimer: Filming in public (particularly of the police) may have legal consequences in some countries. This checklist aims at helping you with logistics and perhaps serving as a reminder for some key components of filming actions, but does not include legal issues.

---

## Before the Action

- [ ] I know who else is filming.
- [ ] I know what kind of video we aim at. (live stream, documentation of repression, footage for action video, footage for documentary)
- [ ] I know where the activity will take place.
- [ ] For action videos: I know higher spots on the route.
- [ ] For repression documentation: I know safe spots with good angles and light.
- [ ] Am I supposed to publish the videos directly, or should I just send them to someone?
- [ ] I know to whom I should send / where to publish the videos.
- [ ] I know when I should send/publish the videos.
- [ ] If you will publish: I know the hashtag of the action.
- [ ] I have a badge/t-shirt to identify me as a media responsible of the organization.
- [ ] I always hold my camera horizontally.
- [ ] For footage filming: I filmed the preparations.
- [ ] For footage filming: I filmed the arrivals of the activists.
- [ ] For footage for documentary: I interviewed some of the activists about what they expect and why they are participating.
- [ ] I am aware of possible police reactions: Do we expect physical confrontation? Should I be saving the videos immediately online, in case police takes away my camera?
- [ ] I am aware of security measures: Are there activists who should not appear in any part of the footage (in case security takes my camera)? Are there activists who must definitely appear in the video (e.g. police violence against members of the parliament, elderlies, or families)?
- [ ] I set up my camera (considering brightness, flash, angle/lenses).
- [ ] My camera has enough battery. (In case of live streaming: I have a back up battery.)
- [ ] I checked the microphone quality of my camera.
- [ ] I know who will be taking photos during the action.
- [ ] I talked with the filming team and I know what they need from me (photographers might take 3-5 second short videos together with the photos).

---

## During the Action

- [ ] I always hold my camera horizontally.
- [ ] From which directions does the light come?
- [ ] We have at least five short videos where the principal slogans are filmed.
- [ ] We have at least one short video where the crowd and the principal banner are filmed.
- [ ] For live streaming and for action video: I filmed the entire action so one can confirm the number of protestors.
- [ ] For action video and for documentary: I have one single footage that could highlight the diversity of the participants.
- [ ] For action video and for documentary: My footages have signs or banners that identify the action. In these, all banners are legible in my photos.
- [ ] We have footage of all banners.
- [ ] For documentary: The activists in my videos do not look distracted or tired. (They may be shouting a slogan, or simply smiling to the camera.)
- [ ] We have close-up and audible videos of all speakers of the demonstration or conference.
- [ ] For photos of speakers: I have videos that show various emotions of a speaker. (anger, joy, determination, cheerfulness etc.)
- [ ] We have some detail footage: people talking to each other, families, an interaction with security authorities, people with colourful costumes or t-shirts with slogans, people holding signs with long phrases, celebrities, etc.
- [ ] I always hold my camera horizontally. (Seriously, do not ever forget this.)
- [ ] If there is a banner drop: I shot a video catching the entire banner drop.
- [ ] If there is confrontation: I filmed the banners that police took away.
- [ ] For repression documentation: I documented police violence and I am in a safe place to protect the footage.
- [ ] For repression videos: My videos include the political message – and not just the physical confrontation itself. (Sometimes a sign or a banner is visible. Sometimes the building entrance has a logo. Sometimes activists have t-shirts that identify them with a cause. Perhaps some verbal messaging from activists. In the worst case, I myself spoke to describe and contextualize the situation.)
- [ ] I am delivering all the urgent videos right away. (The reasons for this may be: An immediate press release must be sent. / Police may take away my camera. / The hashtag is becoming a trending topic and the organizers are expected to feed in.)

---

## After the action

- [ ] I know where my camera is, and if and how I could get it.
- [ ] I got into contact with others of the filming team and confirmed that we have good videos of all kinds.
- [ ] I sent all the videos to the organizers.
- [ ] If you are expected to publish: I made a careful selection of 5-10 short videos that include crowds, banners, slogans, speakers and details; and I published them with a descriptive text and with the right hashtag.
- [ ] If there is an immediate video to be edited: I sent all my photos and short videos to the person who will edit the video.
