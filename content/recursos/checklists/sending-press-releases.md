+++
title = "Checklist - Sending Press Releases"
date = "2023-07-26"
author = "Fermento"
type = "posts"
featured_image = "/images/recursos/checklist-press-release.png"
recursos = "checklists"
weight = "12"
+++

This checklist is about how to send a Press Release, a much simpler task than writing one.

---

## Before

- [ ] I have the final version of the text. / I know where to find the final version of the text.
- [ ] I have the list of emails (of media and journalists, and/or other organizations).
- [ ] I know from which email account to send (and I have access to it): This is important, because some actions involve creation of a separate email account. This can be a false account (to avoid relating individuals to the action) or a common account (as in the case of a protest organized by many groups).
- [ ] If photo/video will be attached, I have the contact of the person who will send them to me.
- [ ] I know at what time to send:
  - [ ] Immediately after an action (e.g. in mass mobilizations)
  - [ ] During an action (e.g. strikes)
  - [ ] At the beginning of an action (e.g. blockades, direct actions)
  - [ ] Early in the morning (e.g. declarations, report releases)

---

## Sending

- [ ] The title of the email is “PRESS RELEASE: [title]”
- [ ] I formatted the text carefully. It should look like this:

{{< figure src="/images/recursos/press-release-format.png" align="center" width="100%" height="auto">}}

- [ ] The email has the logo and website of the organization sending it.
- [ ] Email has contact person information.
- [ ] I am sending always BCC.
- [ ] I am sending in smaller groups of 20 contacts instead of one massive email: This is because large amount of recipients may cause the email to be marked spam. For instance, Riseup servers do not allow for such use.
- [ ] At each re-send, I pay attention to any formatting errors.

---

## After

- [ ]  I prepared an image to accompany online publications.
- [ ]  I published the Press Release on our own website: Sometimes it is better to wait for 1-2 hours before doing this, to see if media covers the story in their own words.
- [ ]  I deleted the contact person information in public posts.
- [ ]  The contact person is ready to receive phone calls.
- [ ]  I follow the news website carefully for 3-5 hours.
- [ ]  I share all news coverage on social media.
- [ ]  Also, I share the full text of the Press Release on all social media accounts that the organization has.
- [ ]  I deleted the contact person information in public posts.
- [ ]  I check email and other social media accounts for if any journalist tries to contact us.
- [ ]  I updated the contact list, deleting the email addresses that bounce back.
