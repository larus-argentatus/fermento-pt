+++
title = "Checklist - Enviar Comunicados de Imprensa"
date = "2023-07-26"
author = "Fermento"
type = "posts"
featured_image = "/images/recursos/checklist-comunicado-imprensa.png"
recursos = "checklists"
weight = "5"
+++


## Antes

- [ ]  Tenho a versão final do texto. / Sei onde encontrar a versão final do texto.
- [ ]  Tenho a lista de endereços de emails a enviar (de media e jornalistas e/ou outras organizações);
- [ ]  Sei a partir de que email devo enviar o comunicado e tenho acesso ao mesmo: isto é importante porque algumas ações envolvem a criação de uma conta de email à parte. Esta conta pode ser falsa (para evitar associar indivíduos às ações) ou contas conjuntas (no caso do protesto ser organizado por muitos grupos).
- [ ]  Se houver alguma foto ou vídeo para ser anexado, tenho o contacto da pessoa que me vai enviar os ficheiros.
- [ ]  Sei quando enviar:
  - [ ]  Imediatamente depois de uma ação (ex: mobilizações de massa)
  - [ ]  Durante uma ação (ex: greves);
  - [ ]  No início de uma ação (ex: bloqueios, ações diretas);
  - [ ]  De manhã cedo (ex: declarações, anúncios, etc.)
---

## Enviar

- [ ] O assunto do email é “COMUNICADO: [título]”;
- [ ] Formatei o texto corretamente. Deve ser algo como:

{{< figure src="/images/recursos/comunicado-formato.png" align="center" width="100%">}}

- [ ] O email tem o logo e site da organização que o está a enviar.
- [ ] O email tem o contacto da pessoa de contacto.
- [ ] Se houver anexos, estão anexados / se houver links ou hiperlinks, ter a certeza que estão a funcionar e corretos.
- [ ] Estou a enviar sempre em BCC.
- [ ] Estou a enviar em grupos pequenos de 20 contactos de cada vez, em vez de enviar um email massivo: isto porque grandes quantidades de recipientes podem fazer com que o email seja marcado como spam. Por exemplo, emails do Riseup não permitem tal uso.
- [ ] De cada vez que reenvio, presto atenção a qualquer erro de formatação.

---

## Depois

- [ ] Prepararei uma imagem para acompanhar as publicações online.
- [ ] Publiquei o comunicado de imprensa no nosso website: às vezes é melhor esperar 1 a 2 horas para ver se os media cobrem a história nas suas próprias palavras).
- [ ] Apaguei a informação de contacto da pessoa de contacto nas publicações públicas.
- [ ] A pessoa de contacto está preparada para receber chamadas telefónicas.
- [ ] Estou a seguir notícias online cuidadosamente durante 3 a 5 horas.
- [ ] Partilho as notícias dos media nas nossas redes sociais.
- [ ] Para além disto, publico o texto completo do comunicado de imprensa em todas as redes sociais da organização.
- [ ] Apaguei a informação da pessoa de contacto em publicações publicas.
- [ ] Vejo o email e outras contas de redes sociais para saber se jornalistas nos tentaram contactar por lá.
- [ ] Atualizei a lista de contactos de imprensa, apagando os emails que voltaram para trás.
