+++
title = "Checklist - Filmar em Ações e Eventos"
date = "2023-07-26"
author = "Fermento"
type = "posts"
featured_image = "/images/recursos/checklist-filmar-acoes.png"
recursos = "checklists"
weight = "4"
+++

Disclaimer: Filmagens públicas (particularmente da polícia) pode ter consequências legais em alguns países. Esta lista visa ajudar-te com a logística e talvez servir como um lembrete para alguns componentes-chave das acções de filmagem, mas não inclui questões legais.

---

## Antes da Ação

- [ ]  Sei quem mais está a filmar.
- [ ]  Sei que tipo de vídeo é pretendido (transmissão em directo, documentação de repressão, filmagens para vídeo de acção, filmagens para documentário).
- [ ]  Sei onde a actividade terá lugar.
- [ ]  Para vídeos de acção: conheço locais elevados ao longo do caminho.
- [ ]  Para documentação de repressão: conheço locais seguros com bons ângulos e boa luz.
- [ ]  Devo publicar os vídeos directamente ou devo enviá-los a alguém?
- [ ]  Sei a quem devo enviar os vídeos / onde publicar os vídeos.
- [ ]  Sei quando devo enviar/publicar os vídeos.
- [ ]  Se vais publicar os vídeos: sei o(s) hashtag(s) da acção.
- [ ]  Tenho um crachá/t-shirt que me identifica enquanto responsável média da organização.
- [ ]  Seguro sempre a minha câmara horizontalmente.
- [ ]  Para filmagens de cenas: filmei os preparativos.
- [ ]  Para filmagens de cenas: filmei a chegada dos activistas.
- [ ]  Filmagens para documentários: Entrevistei alguns dos activistas sobre as suas expectativas e porque motivo estão a participar.
- [ ]  Estou ciente das possíveis reacções policiais: Esperamos confrontos físicos? Devo salvar imediatamente os vídeos online, para o caso da polícia me tirar a câmara?
- [ ]  Estou a par das medidas de segurança: Há activistas que não devem aparecer em nenhuma parte das filmagens (para o caso da segurança me apreender a câmara)? Há activistas que definitivamente devem aparecer no vídeo (e.g. violência policial contra membros do parlamento, cidadãos séniores ou famílias)?
- [ ]  Configurei a minha câmara (considerando luminosidade, brilho, flash, ângulo/lentes).
- [ ]  A minha câmara tem bateria carregada. (Em caso de emissão em directo: tenho uma bateria extra.)
- [ ]  Verifiquei a qualidade do microfone na minha câmara.
- [ ]  Sei quem estará a tirar fotografias durante a acção.
- [ ]  Falei com a equipa de filmagens e sei o que necessitam de minha parte (os fotógrafos podem, juntamente com as fotos, fazer pequenos vídeos de 3 a 5 segundos).

---

## Durante a Ação

- [ ] Seguro sempre na minha câmara horizontalmente.
- [ ] De que direcções vem a luz?
- [ ] Temos pelo menos cinco vídeos curtos em que os principais slogans são filmados.
- [ ] Temos pelo menos um vídeo curto em que a multidão e a faixa principal são filmados.
- [ ] Para emissões em directo e vídeos de acção: filmei a acção completa de forma a que o número de manifestantes possa ser confirmado.
- [ ] Para vídeos de acção e documentários: Tenho uma filmagem única que pode destacar a diversidade de participantes.
- [ ] Para vídeos de acção e documentários: as minhas filmagens têm sinais ou faixas que identificam a acção. Nestes, todas as faixas são legíveis nas minhas fotos.
- [ ] Temos filmagens de todas as faixas.
- [ ] Para documentários: Os activistas nos meus vídeos não parecem distraídos ou cansados (podem estar a gritar palavras de ordem ou simplesmente a sorrir para a câmara).
- [ ] Temos grandes planos e vídeos audíveis de todos os oradores na manifestação ou conferência.
- [ ] Para vídeos de oradores: Tenho vídeos que mostram várias emoções de um orador (raiva, alegria, determinação, etc.).
- [ ] Temos algumas filmagens de detalhes: pessoas a falar umas com as outras, famílias, uma interacção com agentes da autoridade, pessoas com fatos coloridos ou t-shirts com slogans, pessoas a segurar cartazes com frases longas, celebridades, etc.
- [ ] Seguro sempre na minha câmara horizontalmente (a sério, nunca te esqueças disto).
- [ ] Se houver uma largada de faixas: Filmei um vídeo que apanha toda a largada da faixa.
- [ ] Se houver confrontos: filmei as faixas que a polícia apreendeu.
- [ ] Para documentação da repressão: documentei a violência policial e estou num local seguro para proteger a filmagem.
- [ ] Para vídeos de repressão: os meus vídeos incluem a mensagem política – e não apenas a confrontação física per se. (Por vezes um cartaz ou faixa está visível. Por vezes o edifício tem um logótipo. Por vezes os activistas têm t-shirts que os identificam com uma causa. Talvez algumas mensagens verbais por parte dos activistas. No pior caso, eu própria falo para descrever e contextualizar a situação.)
- [ ] Entrego todos os vídeos urgentes imediatamente. (As razões para a urgência podem ser: tem de se enviar um comunicado de imprensa imediato. /A polícia apreendeu a minha câmara. / A hashtag está a tornar-se viral e é esperado que as organizadoras contribuam. )

---

## Após a Reunião

- [ ] Sei onde está a minha câmara, e se e como a posso recuperar.
- [ ] Entrei em contacto com outros da equipa de filmagens e confirmei que temos bons vídeos de todos os tipos.
- [ ] Enviei todos os vídeos aos organizadores.
- [ ] Se é esperado que publiques: fiz uma selecção cuidadosa de 5-10 vídeos curtos que incluem multidões, faixas, slogans, oradores e detalhes; publiquei-os com um texto descritivo e o hashtag correcto.
- [ ] Se há um vídeo imediato a ser editado: enviei todas as fotos e vídeos curtos à pessoa que irá editar o vídeo.
