+++
title = "Checklist - SMAAARRT Objectives"
date = "2023-07-26"
author = "Fermento"
type = "posts"
featured_image = "/images/recursos/checklist-smaaarrt-objectives.png"
recursos = "checklists"
weight = "6"
+++

We use this table to keep track of the objectives we set for actions and campaigns of social movements.

You can download it in PDF format, [here](/recursos/SMAAARRT-objectives-checklist-v2.pdf). To learn about how to use it, check our guidelines at [SMAAARRT Objectives for Activists](/recursos/smaaarrt-objectives-for-activists).

{{< figure src="/images/recursos/SMAAARRT-objectives-checklist-v2.jpg" align="center" width="100%" height="auto">}}
