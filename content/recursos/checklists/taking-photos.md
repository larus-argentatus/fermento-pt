+++
title = "Checklist - Taking Photos in Actions"
date = "2023-07-26"
author = "Fermento"
type = "posts"
featured_image = "/images/recursos/checklist-taking-photos.png"
recursos = "checklists"
weight = "10"
+++

Disclaimer: Taking photos in public (particularly of the police) may have legal consequences in some countries. This checklist aims at helping you with logistics and perhaps serving as a reminder for some key components of taking photos.

---

## Before the Action

- [ ] I know who else is taking photos.
- [ ] I know what kind of photos I should take. (large crowd, landscape, banners, signs, speaker, details, confrontation etc.).
- [ ] I know the route.
- [ ] For crowd photos: I know higher spots on the route.
- [ ] For confrontation photos: I know safe spots with good angles.
- [ ] Am I supposed to publish the photos directly, or should I just send them to someone?
- [ ] I know to whom I should send / where to publish the photos.
- [ ] I know when I should send/publish the photos.
- [ ] If you will publish: I know the hashtag of the action.
- [ ] I have a badge/t-shirt to identify me as a photographer of the organization.
- [ ] I am aware of possible police reactions: Do I have to take photos of a banner before police/security takes it away? Do we expect physical confrontation?
- [ ] I am aware of security measures: Are there activists who should not appear in any of my photos (in case security takes my camera)? Are there activists who must definitely appear in my photos (e.g. police violence against members of the parliament, elderlies, or families)?
- [ ] I set up my camera (considering brightness, flash, angle/lenses).
- [ ] My camera has enough battery.
- [ ] I know who will be filming the action.
- [ ] I talked with the filming team and I know what they need from me (photographers might take 3-5 second short videos together with the photos).


---

## During the Action

- [ ] From which directions does the light come?
- [ ] We have at least five photos where the crowd and the principal banner are visible.
- [ ] For large crowd photos: I took several photos with the principal banner and the demonstrators behind.
- [ ] For large crowd photos: I took several photos where one can confirm the number of protestors.
- [ ] For landscape photos: I took several photos with several banners and signs.
- [ ] For landscape photos: My photos have signs or banners that identify the action.
- [ ] We have photos of all banners.
  - [ ] All banners are legible in my photos.
  - [ ] The banner holders in my photos do not look distracted or tired. (They may be shouting a slogan, or simply smiling to the camera.)
- [ ] We have a selection of photos with various signs.
  - [ ] The sign holders in my photos do not look distracted or tired. (They may be shouting a slogan, or simply smiling to the camera.)
- [ ] We have close-up photos of all speakers of the demonstration.
  - [ ] The photos are not blurred.For photos of speakers: I have photos that show various emotions of a speaker. (anger, joy, determination, cheerfulness etc.).
- [ ] We have some detail photos: people talking to each other, families, an interaction with security authorities, people with colourful costumes or t-shirts with slogans, people holding signs with long phrases, celebrities, etc.
- [ ] If there is a banner drop: I took many horizontal and vertical photos catching the full phrase.
- [ ] If there is confrontation: I took photos of the banners that police took away.
  - [ ] I documented police violence.
  - [ ] My photos include the political message – and not just the physical confrontation itself. (Sometimes a sign or a banner is visible. Sometimes the building entrance has a logo. Sometimes activists have t-shirts that identify them with a cause.).
- [ ] I am delivering all the urgent photos right away. (The reasons for this may be: An immediate press release must be sent. / Police may take away my camera. / The hashtag is becoming a trending topic and the organizers are expected to feed in.).
- [ ] If the filming team requested something: I have enough material for the video.

---

## After the action

- [ ] I know where my camera is, and if and how I could get it.
- [ ] I got into contact with other photographers and confirmed that we have good photos of all kinds (large crowd, landscape, banners, signs, speaker, details, confrontation etc.).
- [ ] I selected some 10-20 photos in good conditions, and I sent them to the organizers separately.
- [ ] I sent all the photos to the organizers.
- [ ] If you are expected to publish: I made a careful selection of 5-10 photos that include crowds, banners, speakers and details; and I published them with a descriptive text and with the right hashtag.
- [ ] If there is an immediate video to be edited: I sent all my photos and short videos to the person who will edit the video.
