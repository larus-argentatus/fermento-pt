+++
title = "Checklist - Escrever Atas de uma Reunião"
date = "2023-07-26"
author = "Fermento"
type = "posts"
featured_image = "/images/recursos/checklist-atas-reuniao.png"
recursos = "checklists"
weight = "2"
+++


## Antes da Reunião

- [ ] Conheço os tópicos e a agenda da reunião.
- [ ] Sei quão detalhadas devem ser as actas: só as decisões e a informação, ou todas as discussões? (no último caso pode ser melhor ter mais do que uma pessoa a tomar notas.)
- [ ] Sei se há um processo de validação: as notas devem ser aprovadas ou modificadas antes de finalizadas?
- [ ] Sei qual é a data limite para entregar a acta.
- [ ] Sei para onde enviar a acta.

---

## Durante a Reunião

- [ ] Tomei nota das presenças (se necessário).
- [ ] Estruturei as notas como tópicos e subtópicos.
- [ ] Interrompo a discussão se estava demasiado rápida para tomar notas.
- [ ] No caso do leitor ter que fazer algo, escrevi **AGIR** (bold e em maiúsculas) seguido da descrição da acção necessária.

> *Exemplo: ”AGIR: Toda a gente preencha este formulário [link] até segunda-feira.”*

- [ ] Para tarefas específicas, escrevi o nome da pessoa a bold.

> *Exemplo: ”Na terça-feira teremos treino de acção. O ****Ernesto**** vai preparar o conteúdo.”*

---

## Após a Reunião

- [ ] Formatei e organizei as notas de forma a que as pessoas que não estiveram presentes na reunião percebam o resumo.
- [ ] No início da acta criei uma secção CHAVE onde compilei a lista de tarefas por pessoa.

> *Exemplo:*
>
> ***“CHAVE***
>
> ***Vladimir**: escrever a proposta de comunicado de imprensa.*
>
> ***Rosa**: preparar o discurso para a acção de quarta-feira e imprimir panfletos.*
>
> ***Ernesto**: preparar o treino de acção, marcar a presença na clínica.*
>
> *etc.”*

- [ ] Enviei a acta ou comecei o processo de validação da mesma.
- [ ] Se houver um processo de validação: actualizei o ficheiro e enviei a versão final.
