+++
title = "Atividade - Formar Grupos de Afinidade"
date = "2023-07-26"
author = "Fermento"
type = "posts"
featured_image = "/images/recursos/atividade-grupos-afinidade.png"
recursos = "atividades"
weight = "2"
+++

Em acções com muitas pessoas, as experiências do passado mostraram que precisamos de criar grupos de afinidade para cuidar umas das outras.

[Aqui](/recursos/Checklist-Grupos-de-Afinidade-v2.pdf) tens uma lista de perguntas para discutires no teu grupo de afinidade.

## Porquê grupos de afinidade?

Durante as acções, é importante teres um grupo de confiança com quem podes contar, para poderem discutir os vossos limites, como se sentem e o que gostariam de fazer. Grupos de afinidade são a base duma acção massiva.

## Temos que estar juntas o tempo todo?

Propomos que formem os grupos de afinidade também para uma reflexão colectiva ao longo das preparações.

Durante a acção, podem subdividir o grupo em pares (_buddies_), que ficarão juntos em todas as circunstâncias. Se algo acontecer a uma de vocês – detenção, ferimento, etc.- o outro par pode apoiar esta pessoa. As outras do grupo de afinidade podem decidir se querem continuar ou ficar. Quando estão no terreno de acção, podem reflectir juntas e decidir.

## Quem são as pessoas certas para o meu grupo?

Muitas vezes, grupos de afinidade são formados com base no nível de experiência. Mas isto não é preciso. O que é mais importante é partilharem uma visão sobre a acção e que haja confiança entre vocês. Quais são as vossas expectativas? Quais são as vossas limitações?

Conhecerem-se uma à outra é essencial para tomarem decisões e para se divertirem. Por isso, investe algum tempo antes da acção para conversarem.
