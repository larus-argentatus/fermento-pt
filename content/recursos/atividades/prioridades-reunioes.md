+++
title = "Atividade - Prioridades nas Reuniões"
date = "2023-07-26"
author = "Fermento"
type = "posts"
featured_image = "/images/recursos/atividade-prioridades-reunioes.png"
recursos = "atividades"
weight = "3"
+++

Utilizamos esta tabela para identificar as expectativas das participantes numa reunião. As expectativas podem condicionar o sítio e a duração da reunião e o tipo da facilitação implementada.

Apresentamos esta tabela na formação [Facilitação de Reuniões – Básica](/modulos-formacoes/facilitacao-reunioes-basica).

{{< figure src="/images/recursos/prioridades-reunioes.jpg" align="center" width="100%" >}}
