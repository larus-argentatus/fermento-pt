+++
title = "Atividade - Avaliar uma Ação Direta"
date = "2023-07-26"
author = "Fermento"
type = "posts"
featured_image = "/images/recursos/atividade-avaliar-acao.png"
recursos = "atividades"
weight = "1"
+++

O processo de fazer uma acção começa quando formas a equipa da acção e continua com as preparações (reuniões, ensaios, sessões de materiais), mas não acaba com a acção em si. Logo depois da acção, devem fazer um _debriefing_, uma avaliação inicial para garantir que toda a gente está okay. Mas o processo fica encerrado quando fazem uma avaliação da acção e do processo inteiro. Aqui ficam algumas perguntas que podem ajudar-vos nesta avaliação.

---

## 0) Temperature check

1.  **Correu bem?** Conseguiram fazer a acção ou falhou alguma coisa antes da concretização?
2.  **Estão bem?** Toda a gente está ok e seguro? Alguém ficou magoada?
3.  **Como foi?** Divertiram-se? Houve algo frustrante?
4.  **Têm tudo?** Faixas, vídeos, outros materiais, estão todos seguros? Quem os tem? Onde estão?

**Antes de aprofundar, pede a uma pessoa para descrever a acção inteira numa forma objectiva.** O que aconteceu? Quem fez o quê? Que reacções receberam (do público, dos trabalhadores, da polícia, dos jornalistas)? Como entraram? Como saíram?

---

## 1) Impacto social

1.  Conseguiram tirar boas **fotos**? Onde estão? Já foram publicadas?
2.  Vai haver **vídeo**? Quem está a editar? Quando vai ser pronto?
3.  O **comunicado** saiu em tempo útil? Tive impacto na comunicação social? Televisão? Imprensa? Online?
4.  Houve jornalistas? Alguém falou com elas? Como foi?
5.  Como foi o impacto nas **redes sociais**? Quantas partilhas ou reacções?

---

## 2) Acção

1.  A acção foi **concretizada** como tinham pensado?
2.  Quem acha que foi um **sucesso**? (uma pergunta muito subjectiva, só para testar o ambiente)
3.  Houve pessoas **identificadas** ou detidas? Quem? Estão ok agora?
4.  Houve pessoas **feridas**? Houve pessoas que ficaram isoladas durante a acção?
5.  Como reagiram **os seguranças** / a polícia?
6.  Como reagiu **o público**?
7.  Os pares (**_buddies_**) funcionaram?
8.  Toda a gente cumpriu o seu **papel**?
9.  Como foi a **comunicação entre as activistas** durante a acção? Toda a gente estive na mesma página?
10.  **Timings**? As pessoas chegaram na hora?
11.  A acção foi **não-violenta**? Houve momentos violentos? Se sim, como controlaram a situação?

---

## 3) Preparação

1.  Tiveram **ensaios** suficientes?
2.  Tiveram **materiais** suficientes?
3.  Faltaram-vos algumas **técnicas** que podiam ser úteis?
4.  Toda a gente **participou** nas preparações? Houve espaço para sugestões?

---

## 4) Processo

1.  Houve **objectivos** que foram claros para toda a gente?
2.  Como foi a **comunicação interna**? As activistas sentiram-se seguras? Tiveram **clareza** sobre o que estava a passar?
3.  Alguém ficou **sobre-carregado**? As tarefas estavam bem-distribuídas entre toda a gente?
4.  **Alguém saiu no meio** do processo? Como foi esta situação tratada? (papéis, pares, frustração etc.)
5.  **Alguém entrou no meio** do processo? Como foi, em termos de papéis, pares, construção de confiança etc=?

---

## 5) E agora?

1.  Como ficaram os **custos** dos materiais? Quem pagou para o quê? Há pessoas que podem precisar de reembolso do colectivo?
2.  Como estão? **Querem fazer mais** coisas deste género (ou maior)?
3.  Há mais **algo marcado para o futuro**? (É completamente normal que não haja neste momento, mas é bom saber se há expectativas no grupo neste sentido.)
